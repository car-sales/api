package md.car.sales.image;

import com.tinify.Options;
import com.tinify.Source;
import com.tinify.Tinify;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ImageStore {

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    public void init() {
        Tinify.setKey("ZDnqYTS70Y5WWTvyXrrjFpPSQkcV9vDd");
    }

    @Transactional
    public void saveCarImages(List<MultipartFile> images, long id) throws IOException {

        int imgNumber = 0;
        for (MultipartFile image : images) {

            ++imgNumber;

            byte[] imgBytes = scale(image.getBytes(), 320, 240);
            byte[] imgBytesBig = scale(image.getBytes(), 1280, 960);

            em.joinTransaction();

            String query1 = "INSERT INTO ad_images_320x240(ad_id,number,image) VALUES(?,?,?)";
            String query2 = "INSERT INTO ad_images_1280x960(ad_id,number,image) VALUES(?,?,?)";
            em.createNativeQuery(query1)
                    .setParameter(1, id)
                    .setParameter(2, imgNumber)
                    .setParameter(3, imgBytes)
                    .executeUpdate();

            em.createNativeQuery(query2)
                    .setParameter(1, id)
                    .setParameter(2, imgNumber)
                    .setParameter(3, imgBytesBig)
                    .executeUpdate();

            em.close();

        }

    }


    @Transactional
    public byte[] getImage(long id) {

        em.joinTransaction();

        try {
            String query = "SELECT image FROM ad_images_320x240 WHERE ad_id=" + id + " AND number=1";

            return (byte[]) em.createNativeQuery(query).getResultList().get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        em.close();
        return null;
    }

    public byte[] scale(byte[] fileData, int width, int height) {
        try {
            Source source = Tinify.fromBuffer(fileData);
            Options options = new Options()
                    .with("method", "fit")
                    .with("width", width)
                    .with("height", height);
            Source resized = source.resize(options);
            return resized.toBuffer();
        } catch (IOException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Transactional
    public List<Object> geImages(long adId) {

        return this.getImageQuery(adId, "ad_images_320x240");
    }

    @Transactional
    public List<Object> getFullSizeImages(long adId) {

        return this.getImageQuery(adId, "ad_images_1280x960");
    }

    private List<Object> getImageQuery(long adId, String tableName) {

        em.joinTransaction();

        String query = "SELECT image FROM " + tableName + " WHERE ad_id=" + adId + " ORDER BY number ASC";

        List<Object> imgBytes = new ArrayList<Object>(em.createNativeQuery(query).getResultList());
        em.close();

        return imgBytes;
    }

}
