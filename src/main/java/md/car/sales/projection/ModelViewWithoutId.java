package md.car.sales.projection;

public interface ModelViewWithoutId {

    String getModelName();

    String getBrand();
}
