package md.car.sales.projection;

import java.util.List;

public interface BrandView {

    long getId();

    String getBrandName();

    List<ModelView> getModels();
}
