package md.car.sales.projection;

public interface CarView {

    ModelViewWithoutId getModel();
}
