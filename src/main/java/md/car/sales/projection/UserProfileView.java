package md.car.sales.projection;

import com.fasterxml.jackson.annotation.JsonIgnore;
import md.car.sales.dto.AdPreviewDTO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public interface UserProfileView {

    long getId();

    String getEmail();

    String getUserName();

    String getFirstName();

    String getLastName();

    String getPhone();

    List<AdPreviewDTO> getLikedAdDtos();

    @JsonIgnore
    Date getCreatedAt();

    default String getDate() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        return format.format(getCreatedAt());
    }
}
