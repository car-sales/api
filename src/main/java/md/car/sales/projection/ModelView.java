package md.car.sales.projection;

public interface ModelView {

    long getId();

    String getModelName();

}
