package md.car.sales.projection;

import com.fasterxml.jackson.annotation.JsonIgnore;
import md.car.sales.model.Car;
import md.car.sales.model.Region;

import java.util.Date;

public interface AdView {

    long getId();

    Car getCar();

    Date getCreatedAt();

    Date getUpdatedAt();

    @JsonIgnore
    UserProfileView getUser();

    Region getRegion();

    boolean isNegotiable();

    String getPhone();

    String getEmail();

    String getDescription();

    String getDealType();

    int getPrice();

    String getCurrency();

//    default List<String> getImages() {
//
//        return new ImageStore().geImages(getId());
//    }

}
