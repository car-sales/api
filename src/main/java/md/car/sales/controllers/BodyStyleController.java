package md.car.sales.controllers;

import md.car.sales.service.BodyStyleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BodyStyleController {

    private BodyStyleService bodyStyleService;

    public BodyStyleController(BodyStyleService bodyStyleService) {
        this.bodyStyleService = bodyStyleService;
    }

    @GetMapping("bodyStyles")
    public ResponseEntity<List<String>> getBodyStyles() {

        return new ResponseEntity<>(bodyStyleService.getBodyStyles(), HttpStatus.OK);
    }
}

