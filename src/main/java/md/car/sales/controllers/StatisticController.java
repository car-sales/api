package md.car.sales.controllers;

import md.car.sales.service.StatisticService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "/statistics")
public class StatisticController {

    private final StatisticService statisticService;

    public StatisticController(StatisticService statisticService) {
        this.statisticService = statisticService;
    }

    @GetMapping("/ads/added/today")
    public ResponseEntity<?> getTodayAddedAdsCount() {
        return new ResponseEntity<>(statisticService.getAddedAdsCount(), HttpStatus.OK);
    }
}
