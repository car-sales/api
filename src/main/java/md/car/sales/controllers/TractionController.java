package md.car.sales.controllers;

import md.car.sales.service.TractionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TractionController {
    private TractionService tractionService;

    public TractionController(TractionService tractionService) {
        this.tractionService = tractionService;
    }

    @GetMapping("traction")
    public ResponseEntity<List<String>> getTraction() {

        return new ResponseEntity<>(tractionService.getTractions(), HttpStatus.OK);
    }
}
