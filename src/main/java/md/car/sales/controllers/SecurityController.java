package md.car.sales.controllers;


import md.car.sales.model.Security;
import md.car.sales.service.SecurityService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SecurityController {

    private SecurityService securityService;

    public SecurityController(SecurityService securityService) {
        this.securityService = securityService;
    }

    @PostMapping("securities")
    public ResponseEntity<?> addSecurities(@RequestBody List<Security> securities) {
        if (securityService.addSecurities(securities) != null)
            return new ResponseEntity<>(HttpStatus.CREATED);
        else return new ResponseEntity<>(HttpStatus.CONFLICT);
    }

    @GetMapping("securities")
    public ResponseEntity<List<Security>> getSecurities() {
        return new ResponseEntity<>(securityService.getSecurities(), HttpStatus.OK);
    }
}
