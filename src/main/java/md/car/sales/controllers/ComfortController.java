package md.car.sales.controllers;

import md.car.sales.model.Comfort;
import md.car.sales.service.ComfortService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ComfortController {

    private ComfortService comfortService;

    public ComfortController(ComfortService comfortService) {
        this.comfortService = comfortService;
    }

    @PostMapping("comforts")
    public ResponseEntity<?> addComforts(@RequestBody List<Comfort> comforts) {
        if (comfortService.addComforts(comforts) != null)
            return new ResponseEntity<>(HttpStatus.CREATED);
        else return new ResponseEntity<>(HttpStatus.CONFLICT);
    }

    @GetMapping("comforts")
    public List<Comfort> getComforts() {
        return comfortService.getComforts();
    }

}
