package md.car.sales.controllers;

import md.car.sales.model.Brand;
import md.car.sales.model.Model;
import md.car.sales.projection.ModelView;
import md.car.sales.service.ModelService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ModelController {

    private ModelService modelService;

    public ModelController(ModelService modelService) {
        this.modelService = modelService;
    }

    @PostMapping("models")
    public ResponseEntity<?> addModels(@RequestBody List<Model> models) {
        modelService.addModels(models);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("models")
    public ResponseEntity<?> addModelsByBrand(@RequestBody Brand brand) {

        try {
            modelService.addModelsByBrand(brand);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("models")
    public ResponseEntity<List<ModelView>> getModels() {
        return new ResponseEntity<>(modelService.getModels(), HttpStatus.OK);
    }

}
