package md.car.sales.controllers;

import md.car.sales.email.EmailSender;
import md.car.sales.email.model.Contact;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ContactController {

    private EmailSender emailSender;

    public ContactController(@Qualifier("contactEmail") EmailSender emailSender) {
        this.emailSender = emailSender;
    }

    @PostMapping("/contact")
    public ResponseEntity<?> sendEmail(@RequestBody Contact contact) {
        try {
            emailSender.setModel(contact);
            emailSender.sendEmail();
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }
}
