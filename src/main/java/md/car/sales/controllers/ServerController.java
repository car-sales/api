package md.car.sales.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
public class ServerController {

    @GetMapping("/server/status")
    public ResponseEntity<?> checkServerStatus() {
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
