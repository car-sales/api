package md.car.sales.controllers;

import md.car.sales.service.CurrencyService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CurrencyController {
    private CurrencyService currencyService;

    public CurrencyController(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @GetMapping("currencies")
    public ResponseEntity<List<String>> getStates() {

        return new ResponseEntity<>(currencyService.getCurrencies(), HttpStatus.OK);
    }
}
