package md.car.sales.controllers;

import md.car.sales.service.DealTypeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DealTypeController {
    private DealTypeService dealTypeService;

    public DealTypeController(DealTypeService dealTypeService) {
        this.dealTypeService = dealTypeService;
    }

    @GetMapping("dealTypes")
    public ResponseEntity<List<String>> getStates() {

        return new ResponseEntity<>(dealTypeService.getDealTypes(), HttpStatus.OK);
    }
}
