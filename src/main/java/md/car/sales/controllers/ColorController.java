package md.car.sales.controllers;

import md.car.sales.service.ColorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ColorController {
    private ColorService colorService;

    public ColorController(ColorService colorService) {
        this.colorService = colorService;
    }

    @GetMapping("colors")
    public ResponseEntity<List<String>> getColors() {

        return new ResponseEntity<>(colorService.getColors(), HttpStatus.OK);
    }
}
