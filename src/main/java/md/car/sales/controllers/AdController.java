package md.car.sales.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import md.car.sales.dto.AdViewDTO;
import md.car.sales.dto.AdsPage;
import md.car.sales.filter.FilterOptions;
import md.car.sales.model.Ad;
import md.car.sales.service.AdService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
public class AdController {

    private AdService adService;

    public AdController(AdService adService) {
        this.adService = adService;
    }

    @PostMapping("ad")
    public ResponseEntity<?> saveAd(@RequestParam("files") List<MultipartFile> files, @RequestParam("ad") String ad,
                                    HttpServletRequest request) {

        try {
            adService.saveAd(files, new ObjectMapper().readValue(ad, Ad.class), request.getHeader("authorization"));
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("ads")
    public ResponseEntity<AdsPage> getAdsBySpecification(
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, value = 24) final Pageable pageable,
            FilterOptions filterOptions) throws IOException {

        return new ResponseEntity<>(adService.getAdsByCriteria(pageable, filterOptions), HttpStatus.OK);

    }

    @GetMapping("ad/{id}")
    public ResponseEntity<AdViewDTO> getAdById(@PathVariable long id) {

        AdViewDTO adViewDTO = adService.getAdById(id);

        return (adViewDTO != null ? new ResponseEntity<>(adViewDTO, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("ad/{id}/bigImages")
    public ResponseEntity<List<Object>> getAdBigImages(@PathVariable long id) {

        return new ResponseEntity<>(adService.getAdBigImages(id), HttpStatus.OK);
    }

    @GetMapping("ads/{id}/views")
    public ResponseEntity<Map<String, Integer>> getAdViews(@PathVariable long id) {
        return new ResponseEntity<>(adService.getAdViews(id), HttpStatus.OK);
    }

    @DeleteMapping("ads/{id}")
    public ResponseEntity<?> removeAdById(@PathVariable String id) {

        try {
            adService.removeAdById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
