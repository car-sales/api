package md.car.sales.controllers;

import md.car.sales.service.StateService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StateController {
    private StateService stateService;

    public StateController(StateService stateService) {
        this.stateService = stateService;
    }

    @GetMapping("states")
    public ResponseEntity<List<String>> getStates() {

        return new ResponseEntity<>(stateService.getStates(), HttpStatus.OK);
    }
}
