package md.car.sales.controllers;

import md.car.sales.service.FuelService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FuelController {

    private FuelService fuelService;

    public FuelController(FuelService fuelService) {
        this.fuelService = fuelService;
    }

    @GetMapping("fuels")
    public ResponseEntity<List<String>> getFuels() {

        return new ResponseEntity<>(fuelService.getFuels(), HttpStatus.OK);
    }
}
