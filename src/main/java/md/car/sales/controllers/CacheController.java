package md.car.sales.controllers;

import md.car.sales.service.CacheService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CacheController {

    private CacheService cacheService;

    public CacheController(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @DeleteMapping("/caches/all")
    public void clearAllCaches() {
        cacheService.evictAllCaches();
    }

    @CacheEvict(value = {"adById", "adIsLiked", "currentUser"}, allEntries = true)
    @DeleteMapping("/caches")
    public void clearCaches() {
    }

}
