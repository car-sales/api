package md.car.sales.controllers;

import md.car.sales.service.TransmissionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TransmissionController {
    private TransmissionService transmissionService;

    public TransmissionController(TransmissionService transmissionService) {
        this.transmissionService = transmissionService;
    }

    @GetMapping("transmissions")
    public ResponseEntity<List<String>> getTransmissions() {

        return new ResponseEntity<>(transmissionService.getTransmissions(), HttpStatus.OK);
    }
}
