package md.car.sales.controllers;

import md.car.sales.dto.UserDTO;
import md.car.sales.dto.UserProfileDTO;
import md.car.sales.email.EmailSender;
import md.car.sales.model.User;
import md.car.sales.projection.UserProfileView;
import md.car.sales.service.CacheService;
import md.car.sales.service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.List;

@RestController
public class UserController {

    private UserService userService;
    private CacheService cacheService;

    private EmailSender emailSender;

    public UserController(UserService userService, CacheService cacheService,
                          @Qualifier("userActivation") EmailSender emailSender) {
        this.userService = userService;
        this.cacheService = cacheService;
        this.emailSender = emailSender;
    }

    @PostMapping("/sign-up")
    public ResponseEntity<?> signUp(@RequestBody User user) {

        if (userService.userExistsByEmail(user.getEmail()))
            return new ResponseEntity<>("This email is already registered", HttpStatus.CONFLICT);

        if (userService.userExistsByUsername(user.getUserName()))
            return new ResponseEntity<>("This user name is busy", HttpStatus.CONFLICT);
        User newUser = null;
        try {
            user.setActive(0);
            newUser = userService.saveUser(user);
            emailSender.setModel(newUser);
            emailSender.sendEmail();

        } catch (DataIntegrityViolationException | MessagingException e) {
            e.printStackTrace();
            if (newUser != null) userService.deleteUserById(newUser.getId());
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/admin/users")
    public ResponseEntity<List<UserProfileView>> users() {
        return new ResponseEntity<>(userService.geUsers(), HttpStatus.OK);
    }

    @GetMapping("/current-user")
    public UserProfileDTO getPrincipalUser() {

        try {
            return userService.getCurrentUser();
        } catch (Exception e) {
            return null;
        }
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<UserDTO> getUserById(@PathVariable long id) {
        try {
            UserDTO user = userService.getUserById(id);
            if (user != null)
                return new ResponseEntity<>(user, HttpStatus.OK);
            else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        }
    }

    @CacheEvict(value = "adIsLiked", key = "#id+#token", allEntries = true)
    @GetMapping("/like/ad/{id}")
    public ResponseEntity<?> likeAnAd(@PathVariable long id, @RequestParam String token) {
        try {
            cacheService.evictCacheByNameAndKey("currentUser", token);
            userService.addLikedAd(id);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/unlike/ad/{id}")
    public ResponseEntity<?> unlikeAnAd(@PathVariable long id, @RequestParam String token) {
        try {
            cacheService.evictCacheByNameAndKey("currentUser", token);
            cacheService.evictCacheByNameAndKey("adIsLiked", id + token);

            userService.unlikeAd(id);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/activate/{id}")
    public ResponseEntity<?> activateAccount(@PathVariable long id) {
        try {
            if (userService.isInactiveById(id)) {
                userService.activateUserAccount(id);
                return new ResponseEntity<>("Successfully activated", HttpStatus.OK);
            } else return new ResponseEntity<>("Already activated", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @Cacheable(value = "adIsLiked", key = "#id+#token")
    @GetMapping("/liked/{id}")
    public Boolean isLikedByUser(@PathVariable long id, @RequestParam String token) {
        try {
            return userService.isLikedByCurrentUser(id);
        } catch (Exception e) {

            return false;
        }
    }
}