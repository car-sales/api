package md.car.sales.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"comfortName"}))
public class Comfort implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String comfortName;

    public Comfort() {
    }

    public Comfort(long id) {
        this.id = id;
    }

    public Comfort(String comfortName) {
        this.comfortName = comfortName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getComfortName() {
        return comfortName;
    }

    public void setComfortName(String comfortName) {
        this.comfortName = comfortName;
    }
}
