package md.car.sales.model;

import java.io.Serializable;

public enum Transmission implements Serializable {

    AUTOMATIC("Automatic"),
    MANUAL("Manual"),
    AUTO_MANUAL("Auto-manual"),
    CVT("CVT"),
    OTHER("Other");

    private String name;

    Transmission(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
