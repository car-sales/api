package md.car.sales.model;

import md.car.sales.converter.CurrencyConverter;
import md.car.sales.converter.DealTypeConverter;
import md.car.sales.model.audit.DateAudit;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Ad extends DateAudit  implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "car_id", nullable = false)
    private Car car;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "region_id")
    private Region region;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean negotiable;

    private String phone;

    private String email;

    @Column(columnDefinition = "text")
    private String description;

    @Convert(converter = DealTypeConverter.class)
    private DealType dealType;

    private int price;

    @Convert(converter = CurrencyConverter.class)
    private Currency currency;

    public Ad() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public boolean getNegotiable() {
        return negotiable;
    }

    public void setNegotiable(boolean negotiable) {
        this.negotiable = negotiable;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDealType() {
        return dealType.getName();
    }

    public void setDealType(String dealType) {
        this.dealType = DealType.valueOf(dealType.toUpperCase());
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getCurrency() {
        return (currency != null ? currency.getName() : null);
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
