package md.car.sales.model;

import java.io.Serializable;

public enum Color implements Serializable {

    BEIGE("Beige"),
    BLACK("Black"),
    BLUE("Blue"),
    BROWN("Brown"),
    GOLD("Gold"),
    GRAY("Gray"),
    GREEN("Green"),
    ORANGE("Orange"),
    PINK("Pink"),
    PURPLE("Purple"),
    RED("Red"),
    SILVER("Silver"),
    WHITE("White"),
    YELLOW("Yellow"),
    OTHER("Other");

    private String name;

    Color(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
