package md.car.sales.model;

import java.io.Serializable;

public enum SteerWheelSide  implements Serializable {
    LEFT("Left"),
    RIGHT("Right");

    private String name;

    SteerWheelSide(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
