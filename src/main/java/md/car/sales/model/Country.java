package md.car.sales.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Country implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String countryName;

    @OneToMany(
            mappedBy = "country",
            fetch = FetchType.EAGER,
            orphanRemoval = true
    )
    private List<Region> regions = new ArrayList<>();

    public Country(String countryName) {
        this.countryName = countryName;
    }

    public Country() {
    }

//    public List<String> getRegions() {
//        return regions.stream().map(Region::getRegionName).collect(toList());
//    }

    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

    public void addRegions(Region region) {
        region.setCountry(this);
        this.regions.add(region);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
