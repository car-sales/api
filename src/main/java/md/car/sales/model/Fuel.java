package md.car.sales.model;

import java.io.Serializable;

public enum Fuel implements Serializable {

    HYBRID("Hybrid"),
    PLUG_IN_HYBRID("Plug-in Hybrid"),
    GASOLINE("Gasoline"),
    DIESEL("Diesel"),
    ELECTRIC("Electric"),
    GAS("Gas");

    private String name;

    Fuel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
}

