package md.car.sales.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
public class Model implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String modelName;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "brand_id")
    private Brand brand;

    public Model() {

    }

    public Model(Brand brand) {
        this.brand = brand;
    }

    public Model(String modelName) {
        this.modelName = modelName;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public long getId() {
        return id;
    }

    public String getBrand() {
        return brand.getBrandName();
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }
}
