package md.car.sales.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import md.car.sales.converter.FuelConverter;

import javax.persistence.*;
import java.io.Serializable;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table(name="engine")
public class Engine implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private int horsePower;

    @Column(columnDefinition = "decimal(2,1)")
    private double capacity;

    @Convert(converter = FuelConverter.class)
    private Fuel fuel;

    public Engine() {
    }

    public Engine(int horsePower, int capacity, Fuel fuel) {
        this.horsePower = horsePower;
        this.capacity = capacity;
        this.fuel = fuel;
    }

    public String getFuel() {
        return fuel.getName();
    }

    public void setFuel(String fuel) {
        this.fuel = Fuel.valueOf(fuel.toUpperCase()
                .replace("-","_")
                .replace(" ","_"));
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }
}
