package md.car.sales.model;

import java.io.Serializable;

public enum DealType implements Serializable {

    SALE("Sale"),
    BUY("Buy"),
    CHANGE("Change");

    private String name;

    DealType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}