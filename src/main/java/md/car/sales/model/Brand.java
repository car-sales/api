package md.car.sales.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"brandName"}))
public class Brand implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String brandName;

    @OneToMany(
            mappedBy = "brand",
            cascade = CascadeType.MERGE
    )
    private List<Model> models = new ArrayList<>();

    public Brand(String brandName, List<Model> models) {
        this.models = models;
        this.brandName = brandName;
    }

    public Brand(String brandName) {
        this.brandName = brandName;
    }

    public Brand() {

    }

    public String getBrandName() {
        return brandName;
    }

    public List<Model> getModels() {
        return this.models;
//        return models.stream().map(Model::getModelName).collect(toList());
    }

    public void addModels(Model model) {
        model.setBrand(this);
        this.models.add(model);
    }

    public void setModels(List<Model> models) {
        this.models = models;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

}
