package md.car.sales.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import md.car.sales.converter.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

@Entity
public class Car implements Serializable {

    @Transient
    @JsonIgnore
    private static final long serialVersionUID = -3636706560579339074L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private int mileage;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "model_id")
    private Model model;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "engine_id")
    private Engine engine;

    @Convert(converter = TractionConverter.class)
    private Traction traction;

    @Convert(converter = TransmissionConverter.class)
    private Transmission transmission;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "car_security",
            joinColumns = @JoinColumn(name = "car_id"),
            inverseJoinColumns = @JoinColumn(name = "security_id"))
    private Set<Security> securities = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "car_comfort",
            joinColumns = @JoinColumn(name = "car_id"),
            inverseJoinColumns = @JoinColumn(name = "comfort_id"))
    private Set<Comfort> comforts = new HashSet<>();

    @Convert(converter = StateConverter.class)
    private State state;

    private int doorsNumber;

    private int year;

    @Convert(converter = BodyStyleConverter.class)
    private BodyStyle bodyStyle;

    @Convert(converter = ColorConverter.class)
    private Color interiorColor;

    @Convert(converter = ColorConverter.class)
    private Color exteriorColor;

    @Convert(converter = SteerWheelSideConverter.class)
    private SteerWheelSide steerWheelSide;

    public Car() {
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public String getSteerWheelSide() {
        return steerWheelSide.getName();
    }

    public void setSteerWheelSide(String steerWheelSide) {
        this.steerWheelSide = SteerWheelSide.valueOf(steerWheelSide.toUpperCase());
    }

    public List<String> getSecurities() {
        return securities.stream().map(Security::getSecurityName).collect(toList());
    }

    public void addSecurity(Security security) {
        this.securities.add(security);
    }

    public Set<Comfort> getComforts() {
        return comforts;
    }

    public void addComfort(Comfort comfort) {
        this.comforts.add(comfort);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public String getTraction() {
        return traction.getName();
    }

    public void setTraction(String traction) {
        switch (traction) {
            case "4x2":
                traction = "AWD_TWO_WHEEL_DRIVE";
                break;
            case "4x4":
                traction = "AWD_FOUR_WHEEL_DRIVE";
                break;
        }
        this.traction = Traction.valueOf(traction.toUpperCase().replace("-", "_")
                .replace(" ", "_"));
    }

    public String getTransmission() {
        return transmission.getName();
    }

    public void setTransmission(String transmission) {
        this.transmission = Transmission.valueOf(transmission.toUpperCase().replace("-", "_")
                .replace(" ", "_"));
    }

    public void setSecurities(Set<Security> securities) {
        this.securities = securities;
    }

    public void setComforts(Set<Comfort> comforts) {
        this.comforts = comforts;
    }

    public String getState() {
        return state.getName();
    }

    public void setState(String state) {
        this.state = State.valueOf(state.replace(" ", "_").toUpperCase());
    }

    public String getBodyStyle() {
        return bodyStyle.getName();
    }

    public void setBodyStyle(String bodyStyle) {
        this.bodyStyle = BodyStyle.valueOf(bodyStyle.replace(" ", "_").toUpperCase());
    }

    public String getInteriorColor() {
        return interiorColor.getName();
    }

    public void setInteriorColor(String interiorColor) {
        this.interiorColor = Color.valueOf(interiorColor.toUpperCase());
    }

    public String getExteriorColor() {
        return exteriorColor.getName();
    }

    public void setExteriorColor(String exteriorColor) {
        this.exteriorColor = Color.valueOf(exteriorColor.toUpperCase());
    }

    public int getDoorsNumber() {
        return doorsNumber;
    }

    public void setDoorsNumber(int doorsNumber) {
        this.doorsNumber = doorsNumber;
    }

}
