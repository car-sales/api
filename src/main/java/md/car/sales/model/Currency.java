package md.car.sales.model;

import java.io.Serializable;

public enum Currency implements Serializable {

    USD("$"),
    EUR("€"),
    MDL("MDL"),
    RON("RON");

    private String name;

    Currency(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
