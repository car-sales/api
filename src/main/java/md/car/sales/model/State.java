package md.car.sales.model;

import java.io.Serializable;

public enum State implements Serializable {
    USED("Used"),
    NEW("New"),
    NEEDS_REPAIR("Needs repair");

    private String name;

    State(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
