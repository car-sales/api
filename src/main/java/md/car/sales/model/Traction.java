package md.car.sales.model;

import java.io.Serializable;

public enum Traction  implements Serializable {

    AWD("AWD"),
    RWD("RWD"),
    FWD("FWD"),
    AWD_TWO_WHEEL_DRIVE("4x2"),
    AWD_FOUR_WHEEL_DRIVE("4x4");

    private String name;

    Traction(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
