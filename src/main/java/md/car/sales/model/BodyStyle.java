package md.car.sales.model;

import java.io.Serializable;

public enum BodyStyle implements Serializable {

    CARGO_VAN("Cargo Van"),
    CONVERTIBLE("Convertible"),
    COUPE("Coupe"),
    PICKUP("Pickup"),
    HATCHBACK("Hatchback"),
    MINIVAN("Minivan"),
    PASSENGER_VAN("Passenger Van"),
    SUV("Suv"),
    SEDAN("Sedan"),
    WAGON("Wagon");

    private String name;

    BodyStyle(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
