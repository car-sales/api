package md.car.sales.repository;

import md.car.sales.model.Ad;
import md.car.sales.model.User;
import md.car.sales.projection.UserProfileView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);

    User findAllByEmail(String email);

    User findByUserName(String userName);

    List<UserProfileView> findAllBy();

    User findAllById(long id);

    User findAllByIdAndLikedAdsIsContaining(long id, Ad ad);
}
