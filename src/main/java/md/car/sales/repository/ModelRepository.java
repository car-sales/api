package md.car.sales.repository;

import md.car.sales.model.Model;
import md.car.sales.projection.ModelView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ModelRepository extends JpaRepository<Model, Long> {

    Model findById(long id);

    ModelView findAllById(long id);

    List<ModelView> findAllBy();
}
