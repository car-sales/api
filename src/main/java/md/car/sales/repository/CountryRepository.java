package md.car.sales.repository;

import md.car.sales.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {

    Country findById(long id);

    Country findAllByCountryName(String countryName);

}
