package md.car.sales.repository;

import md.car.sales.filter.FilterOptions;
import md.car.sales.model.Ad;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdRepository extends PagingAndSortingRepository<Ad, Long> {

//    @Query(
//            value = "SELECT a.*\n" +
//                    "\n" +
//                    "FROM ad a\n" +
//                    "         JOIN region r ON a.region_id = r.id\n" +
//                    "         JOIN car ca ON a.car_id = ca.id\n" +
//                    "         JOIN car_comfort cc ON ca.id = cc.car_id\n" +
//                    "         JOIN car_security cs ON ca.id = cs.car_id\n" +
//                    "         JOIN engine e ON ca.engine_id = e.id\n" +
//                    "         JOIN comfort co ON cc.comfort_id = co.id\n" +
//                    "         JOIN security se ON cs.security_id = se.id\n" +
//                    "         JOIN model m ON ca.model_id = m.id\n" +
//                    "         JOIN brand br ON br.id = m.brand_id\n" +
//                    "WHERE (((:#{#filter.models}) IS NULL) OR (m.model_name IN (:#{#filter.models})))\n" +
//                    "  AND (((:#{#filter.regions}) IS NULL) OR (r.region_name IN (:#{#filter.regions})))\n" +
//                    "  AND (((:#{#filter.dealTypes}) IS NULL) OR (a.deal_type IN (:#{#filter.dealTypes})))\n" +
//                    "  AND (((:#{#filter.fuels}) IS NULL) OR (e.fuel IN (:#{#filter.fuels})))\n" +
//                    "  AND (((:#{#filter.bodyStyles}) IS NULL) OR (ca.body_style IN (:#{#filter.bodyStyles})))\n" +
//                    "  AND (((:#{#filter.interiorColors}) IS NULL) OR (ca.interior_color IN (:#{#filter.interiorColors})))\n" +
//                    "  AND (((:#{#filter.exteriorColors}) IS NULL) OR (ca.exterior_color IN (:#{#filter.exteriorColors})))\n" +
//                    "  AND (((:#{#filter.traction}) IS NULL) OR (ca.traction IN (:#{#filter.traction})))\n" +
//                    "  AND (((:#{#filter.transmissions}) IS NULL) OR (ca.transmission IN (:#{#filter.transmissions})))\n" +
//                    "  AND (((:#{#filter.steerWheelSides}) IS NULL) OR (ca.steer_wheel_side IN (:#{#filter.steerWheelSides})))\n" +
//                    "  AND (((:#{#filter.doorsNumber}) IS NULL) OR (ca.doors_number IN (:#{#filter.doorsNumber})))\n" +
//                    "  AND (((:#{#filter.states}) IS NULL) OR (ca.state IN (:#{#filter.states})))" +
//                    "\n" +
//                    "\n" +
//
//                    "  AND ((e.capacity BETWEEN :#{#filter.capacityStart} AND :#{#filter.capacityEnd}) " +
//                    "  OR ((:#{#filter.getCapacityStart()}=0) AND (:#{#filter.getCapacityEnd()}=0))\n" +
//
//                    "  AND (ca.year BETWEEN :#{#filter.getYearStart()} AND :#{#filter.getYearEnd()})" +
//                    "  OR ((:#{#filter.getYearStart()}=0) AND (:#{#filter.getYearEnd()}=0))\n" +
//
//                    "  AND (e.horse_power BETWEEN :#{#filter.getHorsePowerStart()} AND :#{#filter.getHorsePowerEnd()})" +
//                    "  OR ((:#{#filter.getHorsePowerStart()}=0) AND (:#{#filter.getHorsePowerEnd()}=0))\n" +
//
//                    "  AND (ca.mileage BETWEEN :#{#filter.getMileageStart()} AND :#{#filter.getMileageEnd()})" +
//                    "  OR ((:#{#filter.getMileageStart()}=0) AND (:#{#filter.getMileageEnd()}=0)))\n" +
//
//                    "\n" +
//                    "\n" +
//                    "  AND (((a.negotiable = :#{#filter.isNegotiable()} AND a.price = -1)) OR (a.currency = 2 AND a.price BETWEEN :#{#filter.prices['priceStartInEUR']} AND :#{#filter.prices['priceEndInEUR']}) OR (a.currency = 1 AND a.price BETWEEN :#{#filter.prices['priceStartInUSD']} AND :#{#filter.prices['priceEndInUSD']}) OR (a.currency = 3 AND a.price BETWEEN :#{#filter.prices['priceStartInMDL']} AND :#{#filter.prices['priceEndInMDL']}))\n" +
//
//                    "GROUP BY a.id\n" +
//                    "HAVING (string_agg(DISTINCT co.comfort_name, ',') like :#{#filter.comforts})" +
//                    " AND (string_agg(DISTINCT se.security_name, ',') like :#{#filter.securities})",
//            nativeQuery = true)

    @Query(
            value = "SELECT a.id,a.price,a.currency,a.negotiable,m.model_name, string_agg(distinct (br.brand_name), ','),\n" +
                    "       string_agg(distinct (ai.image), ',') as images\n" +
                    "\n" +
                    "FROM ad a\n" +
                    "         JOIN region r ON a.region_id = r.id\n" +
                    "         JOIN car ca ON a.car_id = ca.id\n" +
                    "         JOIN car_comfort cc ON ca.id = cc.car_id\n" +
                    "         JOIN car_security cs ON ca.id = cs.car_id\n" +
                    "         JOIN engine e ON ca.engine_id = e.id\n" +
                    "         JOIN comfort co ON cc.comfort_id = co.id\n" +
                    "         JOIN security se ON cs.security_id = se.id\n" +
                    "         JOIN model m ON ca.model_id = m.id\n" +
                    "         JOIN brand br ON br.id = m.brand_id\n" +
                    "         JOIN ad_images_320x240 ai ON ai.ad_id = a.id\n" +
                    "WHERE (((:#{#filter.models}) IS NULL) OR (m.model_name IN (:#{#filter.models})))\n" +
                    "  AND (((:#{#filter.regions}) IS NULL) OR (r.region_name IN (:#{#filter.regions})))\n" +
                    "  AND (((:#{#filter.dealTypes}) IS NULL) OR (a.deal_type IN (:#{#filter.dealTypes})))\n" +
                    "  AND (((:#{#filter.fuels}) IS NULL) OR (e.fuel IN (:#{#filter.fuels})))\n" +
                    "  AND (((:#{#filter.bodyStyles}) IS NULL) OR (ca.body_style IN (:#{#filter.bodyStyles})))\n" +
                    "  AND (( (:#{#filter.interiorColors}) IS NULL) OR (ca.interior_color IN (:#{#filter.interiorColors})))\n" +
                    "  AND (((:#{#filter.exteriorColors}) IS NULL) OR (ca.exterior_color IN (:#{#filter.exteriorColors})))\n" +
                    "  AND (((:#{#filter.traction}) IS NULL) OR (ca.traction IN (:#{#filter.traction})))\n" +
                    "  AND (((:#{#filter.transmissions}) IS NULL) OR (ca.transmission IN (:#{#filter.transmissions})))\n" +
                    "  AND (((:#{#filter.steerWheelSides}) IS NULL) OR (ca.steer_wheel_side IN (:#{#filter.steerWheelSides})))\n" +
                    "  AND (((:#{#filter.doorsNumber}) IS NULL) OR (ca.doors_number IN (:#{#filter.doorsNumber})))\n" +
                    "  AND (((:#{#filter.states}) IS NULL) OR (ca.state IN (:#{#filter.states})))" +
                    "\n" +
                    "\n" +

                    "AND ai.number = 1" +

                    "  AND ((e.capacity BETWEEN :#{#filter.capacityStart} AND :#{#filter.capacityEnd}) " +
                    "  OR ((:#{#filter.getCapacityStart()}=0) AND (:#{#filter.getCapacityEnd()}=0)))\n" +

                    "  AND ((ca.year BETWEEN :#{#filter.getYearStart()} AND :#{#filter.getYearEnd()})" +
                    "  OR ((:#{#filter.getYearStart()}=0) AND (:#{#filter.getYearEnd()}=0)))\n" +

                    "  AND ((e.horse_power BETWEEN :#{#filter.getHorsePowerStart()} AND :#{#filter.getHorsePowerEnd()})" +
                    "  OR ((:#{#filter.getHorsePowerStart()}=0) AND (:#{#filter.getHorsePowerEnd()}=0)))\n" +

                    "  AND ((ca.mileage BETWEEN :#{#filter.getMileageStart()} AND :#{#filter.getMileageEnd()})" +
                    "  OR ((:#{#filter.getMileageStart()}=0) AND (:#{#filter.getMileageEnd()}=0)))\n" +

                    "\n" +
                    "\n" +
                    "  AND (((a.negotiable = :#{#filter.isNegotiable()} AND a.price = -1)) OR (a.currency = 2 AND a.price BETWEEN :#{#filter.prices['priceStartInEUR']} AND :#{#filter.prices['priceEndInEUR']}) OR (a.currency = 1 AND a.price BETWEEN :#{#filter.prices['priceStartInUSD']} AND :#{#filter.prices['priceEndInUSD']}) OR (a.currency = 3 AND a.price BETWEEN :#{#filter.prices['priceStartInMDL']} AND :#{#filter.prices['priceEndInMDL']}))\n" +

                    "GROUP BY a.id, a.price, a.currency, a.negotiable,m.model_name\n" +
                    "HAVING (string_agg(DISTINCT co.comfort_name, ',') like :#{#filter.comforts})" +
                    " AND (string_agg(DISTINCT se.security_name, ',') like :#{#filter.securities})",
            nativeQuery = true)
    Page<Object[]> findAll(@Param("filter") FilterOptions filter,
                           Pageable pageable);

    Ad findAllById(long id);

    Ad findById(long id);

    List<Ad> findAllByUserId(long id);

    void deleteAdById(long id);

}
