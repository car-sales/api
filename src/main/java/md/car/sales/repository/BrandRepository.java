package md.car.sales.repository;

import md.car.sales.model.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BrandRepository extends JpaRepository<Brand,Long> {

    List<Brand> findAllByOrderByBrandNameAsc();

    Brand findAllByBrandName(String brandName);

    Brand findById(long id);

}
