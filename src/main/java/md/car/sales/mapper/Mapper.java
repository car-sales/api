package md.car.sales.mapper;

import md.car.sales.converter.CurrencyConverter;
import md.car.sales.dto.*;
import md.car.sales.image.ImageStore;
import md.car.sales.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class Mapper {

    @Autowired
    private ImageStore imageStore;

    private CurrencyConverter currencyConverter = new CurrencyConverter();

    public AdPreviewDTO toAdPreviewDto(Object[] ad) {

        AdPreviewDTO adPreviewDTO = new AdPreviewDTO();

        adPreviewDTO.setId(((BigInteger) ad[0]).longValue());
        adPreviewDTO.setBrand((String) ad[5]);
        adPreviewDTO.setModel((String) ad[4]);
        adPreviewDTO.setCurrency(currencyConverter.convertToEntityAttribute((Integer) ad[2]).getName());
        adPreviewDTO.setPrice((Integer) ad[1]);
        adPreviewDTO.setNegotiable(((Integer) ad[3]) == 1);
        adPreviewDTO.setImage((byte[]) ad[6]);

        return adPreviewDTO;
    }

    public AdPreviewDTO toAdDto(Ad ad) {
        AdPreviewDTO adPreviewDTO = new AdPreviewDTO();

        adPreviewDTO.setId(ad.getId());
        adPreviewDTO.setBrand(ad.getCar().getModel().getBrand());
        adPreviewDTO.setModel(ad.getCar().getModel().getModelName());
        adPreviewDTO.setCurrency(ad.getCurrency());
        adPreviewDTO.setPrice(ad.getPrice());
        adPreviewDTO.setNegotiable(ad.getNegotiable());
        adPreviewDTO.setImage(imageStore.getImage(ad.getId()));
        return adPreviewDTO;
    }

    public AdViewDTO toAdViewDTO(Ad ad) {
        AdViewDTO adViewDTO = new AdViewDTO();
        adViewDTO.setId(ad.getId());
        adViewDTO.setCar(ad.getCar());
        adViewDTO.setCurrency(ad.getCurrency());
        adViewDTO.setPrice(ad.getPrice());
        adViewDTO.setNegotiable(ad.getNegotiable());
        adViewDTO.setCreatedAt(Date.from(ad.getCreatedAt()));
        adViewDTO.setDealType(ad.getDealType());
        adViewDTO.setDescription(ad.getDescription());
        adViewDTO.setEmail(ad.getEmail());
        adViewDTO.setPhone(ad.getPhone());
        adViewDTO.setRegion(ad.getRegion());
        adViewDTO.setUser(this.toUserDTO(ad.getUser()));
        adViewDTO.setImages(imageStore.geImages(ad.getId()));

        return adViewDTO;
    }

    public UserDTO toUserDTO(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setEmail(user.getEmail());
        userDTO.setPhone(user.getPhone());
        userDTO.setUserName(user.getUserName());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());

        return userDTO;
    }

    public UserProfileDTO toUserProfileDTO(User user, List<Ad> userAds) {
        UserProfileDTO userProfileDTO = new UserProfileDTO();
        userProfileDTO.setId(user.getId());
        userProfileDTO.setFirstName(user.getFirstName());
        userProfileDTO.setLastName(user.getLastName());
        userProfileDTO.setUserName(user.getUserName());
        userProfileDTO.setPhone(user.getPhone());
        userProfileDTO.setEmail(user.getEmail());
        userProfileDTO.setCreatedAt(Date.from(user.getCreatedAt()));
        userProfileDTO.setLikedAdsDto(user.getLikedAds().stream().map(this::toAdDto).collect(Collectors.toList()));
        userProfileDTO.setUserAdsDto(userAds.stream().map(this::toAdDto).collect(Collectors.toList()));

        return userProfileDTO;
    }

    public CountryDTO toCountryDto(Country country) {
        CountryDTO countryDto = new CountryDTO();

        countryDto.setId(country.getId());
        countryDto.setCountryName(country.getCountryName());
        countryDto.setRegions(country.getRegions().stream().map(this::toRegionsDto).collect(Collectors.toList()));

        return countryDto;
    }

    private RegionDTO toRegionsDto(Region region) {
        RegionDTO regionDto = new RegionDTO();

        regionDto.setId(region.getId());
        regionDto.setRegionName(region.getRegionName());

        return regionDto;
    }

    public BrandDTO toBrandDto(Brand brand) {
        BrandDTO brandDTO = new BrandDTO();
        brandDTO.setId(brand.getId());
        brandDTO.setBrandName(brand.getBrandName());
        brandDTO.setModels(brand.getModels().stream().map(this::toModelDTO).collect(Collectors.toList()));

        return brandDTO;
    }

    private ModelDTO toModelDTO(Model model) {
        ModelDTO modelDTO = new ModelDTO();
        modelDTO.setId(model.getId());
        modelDTO.setModelName(model.getModelName());

        return modelDTO;
    }

}
