package md.car.sales.email;

import org.springframework.stereotype.Component;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;

@Component
public abstract class EmailSender extends Email {

    protected Object model;

    public void setModel(Object model) {
        this.model = model;
    }

    protected void setRecipient(String recipient) throws MessagingException {
        generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
    }

    protected void setSubject(String subject) throws MessagingException {
        generateMailMessage.setSubject(subject);
    }

    protected void setContent(String content) throws MessagingException {
        generateMailMessage.setContent(content, "text/html");
    }

    public abstract void sendEmail() throws MessagingException;

    protected void send() throws MessagingException {
        Transport transport = getMailSession.getTransport("smtp");

        transport.connect("smtp.gmail.com", "car.sales.bot@gmail.com", "car.sales07012000");
        transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
        transport.close();
    }
}
