package md.car.sales.email.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Contact {

    private String name;
    private String email;
    private String message;

    public Contact() {
    }
}
