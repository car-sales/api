package md.car.sales.email.impl;

import md.car.sales.email.EmailSender;
import md.car.sales.email.model.Contact;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;

@Qualifier("contactEmail")
@Component
public class ContactEmailSender extends EmailSender {

    public void sendEmail() throws MessagingException {

        Contact contact = (Contact) model;

        String emailContent = "<h3>Name: " + contact.getName() + "</h3><p><h3>Email: "
                + contact.getEmail() + " </h3></p> <br<br> " + contact.getMessage();

        setRecipient("ciolacu.ion07@gmail.com");
        setSubject("Contact message Car Sales");
        setContent(emailContent);
        send();
    }
}
