package md.car.sales.email.impl;

import md.car.sales.email.EmailSender;
import md.car.sales.model.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;

@Qualifier("userActivation")
@Component
public class UserActivationEmailSender extends EmailSender {

    @Value("${spring.email.link}")
    protected String activationLink;

    @Override
    public void sendEmail() throws MessagingException {
        User user = (User) model;
        String emailContent = "<html>\n" +
                "  <head>\n" +
                "    <style>\n" +
                "      .button:hover {\n" +
                "        cursor: pointer !important; \n" +
                "      }\n" +
                "      .button {\n" +
                "        font-size: 18px;\n" +
                "        text-decoration: none;\n" +
                "        color: #ffffff;\n" +
                "        background-color: #0003c5;\n" +
                "        padding: 15px 20px 15px 20px;\n" +
                "        border-radius: 5px;\n" +
                "        width: 150px;\n" +
                "        border: none;\n" +
                "      }\n" +
                "    </style>\n" +
                "  </head>\n" +
                "  <body>\n" +
                "    <div>\n" +
                "      <h2>Hi " + user.getFirstName() + " " + user.getLastName() + "</h2>\n" +
                "      <br />\n" +
                "\n" +
                "      Click the button to activate your account\n" +
                "      <br /><br />\n" +
                "      <a href=\"" + activationLink + "activate/" + user.getId() + "\">\n" +
                "        <button class=\"button\">\n" +
                "          <span>Activate</span>\n" +
                "        </button>\n" +
                "      </a>\n" +
                "    </div>\n" +
                "  </body>\n" +
                "</html>\n";

        setRecipient(user.getEmail());
        setSubject("Account activation Car Sales");
        setContent(emailContent);
        send();
    }
}
