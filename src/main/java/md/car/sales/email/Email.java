package md.car.sales.email;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class Email {

    protected Properties mailServerProperties;
    protected Session getMailSession;
    protected MimeMessage generateMailMessage;

    public Email() {
        mailServerProperties = System.getProperties();
        mailServerProperties.put("mail.smtp.port", "587");
        mailServerProperties.put("mail.smtp.auth", "true");
        mailServerProperties.put("mail.smtp.starttls.enable", "true");

        getMailSession = Session.getDefaultInstance(mailServerProperties, null);
        generateMailMessage = new MimeMessage(getMailSession);
    }

}
