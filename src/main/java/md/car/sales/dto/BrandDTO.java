package md.car.sales.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class BrandDTO implements Serializable {

    private long id;
    private String brandName;
    private List<ModelDTO> models;

}
