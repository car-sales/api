package md.car.sales.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import md.car.sales.model.Car;
import md.car.sales.model.Region;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class AdViewDTO implements Serializable {

    @Transient
    @JsonIgnore
    private static final long serialVersionUID = -4299179667007096071L;

    private long id;

    private Car car;

    private Date createdAt;
    private Date UpdatedAt;
    private UserDTO user;

    private Region region;

    private boolean isNegotiable;
    private String phone;
    private String email;
    private String description;
    private String dealType;
    private int price;
    private String currency;
    private List<Object> images = new ArrayList<>();

}
