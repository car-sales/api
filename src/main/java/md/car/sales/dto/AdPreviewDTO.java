package md.car.sales.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class AdPreviewDTO implements Serializable {

    private long id;
    private String brand;
    private String model;
    private Integer price;
    private String currency;
    private boolean negotiable;
    private byte[] image;

}
