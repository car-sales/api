package md.car.sales.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO implements Serializable {

    private long id;
    private String email;
    private String userName;
    private String firstName;
    private String lastName;
    private String phone;


}
