package md.car.sales.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CountryDTO implements Serializable {

    private long id;
    private String countryName;
    private List<RegionDTO> regions = new ArrayList<>();

}
