package md.car.sales.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Transient;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class RegionDTO implements Serializable {

    @Transient
    @JsonIgnore
    private static final long serialVersionUID = 6227380449100319077L;

    private long id;
    private String regionName;

}
