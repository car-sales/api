package md.car.sales.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class UserProfileDTO implements Serializable {
    private long id;
    private String email;
    private String userName;
    private String firstName;
    private String lastName;
    private String phone;
    private List<AdPreviewDTO> likedAdsDto;
    private List<AdPreviewDTO> userAdsDto;
    private String createdAt;

    public void setCreatedAt(Date createdAt) {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

        this.createdAt = format.format(createdAt);
    }
}
