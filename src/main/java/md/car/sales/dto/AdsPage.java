package md.car.sales.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class AdsPage {

    private List<AdPreviewDTO> content;
    private int number;
    private int pageSize;
    private int numOfElements;
    private int totalPages;
    private int adsAddedToday;

}
