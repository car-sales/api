package md.car.sales.currency;

import com.google.gson.Gson;
import lombok.Getter;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

//@RefreshScope
@Getter
@Configuration
public class DailyCurrency {

    //    @Value("${daily_currency.access_key}")
    private String accessKey = "5105204b812e0ab488da5c26be6c39d9";

    //    @Value("${daily_currency.url_str}")
    private String urlStr = "http://data.fixer.io/api/latest?access_key=5105204b812e0ab488da5c26be6c39d9&symbols=USD,MDL,RON&format=1";

    private HashMap<String, Double> convertedCurrencies = new HashMap<>();

    public DailyCurrency() {

    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getUrlStr() {
        return urlStr;
    }

    public void setUrlStr(String urlStr) {
        this.urlStr = urlStr;
    }

    public Double getConvertedCurrency(String currencyToConvert) {

        return convertedCurrencies.get(currencyToConvert);
    }

    @Scheduled(fixedRate = 86400000)
    private void doCurrencyConversion() throws IOException {

        URL url = new URL(urlStr);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.connect();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder content = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();

        CurrencyResponse response = new Gson().fromJson(content.toString(), CurrencyResponse.class);
        Map<String, Double> rates = response.getRates();

        convertedCurrencies.put("EURtoMDL", rates.get("MDL"));
        convertedCurrencies.put("EURtoRON", rates.get("RON"));
        convertedCurrencies.put("EURtoUSD", rates.get("USD"));

        fillConvertedCurrenciesMap(convertedCurrencies.get("EURtoMDL"),
                convertedCurrencies.get("EURtoRON"),
                convertedCurrencies.get("EURtoUSD"));

        System.out.println();
        convertedCurrencies.forEach((key, value) -> System.out.println(key + ":" + value));

    }

    private void fillConvertedCurrenciesMap(Double EURtoMDL, Double EURtoRON, Double EURtoUSD) {

        convertedCurrencies.put("USDtoMDL", EURtoMDL / EURtoUSD);
        convertedCurrencies.put("USDtoEUR", 1 / EURtoUSD);
        convertedCurrencies.put("USDtoRON", EURtoRON / EURtoUSD);
        convertedCurrencies.put("RONtoMDL", EURtoMDL / EURtoRON);
        convertedCurrencies.put("RONtoEUR", 1 / EURtoRON);
        convertedCurrencies.put("RONtoUSD", EURtoUSD / EURtoRON);
        convertedCurrencies.put("MDLtoEUR", 1 / EURtoMDL);
        convertedCurrencies.put("MDLtoUSD", EURtoUSD / EURtoMDL);
        convertedCurrencies.put("MDLtoRON", EURtoRON / EURtoMDL);
    }
}
