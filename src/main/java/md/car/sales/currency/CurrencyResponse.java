package md.car.sales.currency;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
public class CurrencyResponse {

    private boolean success;
    private String base;
    private String date;
    private Map<String, Double> rates;

}
