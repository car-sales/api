package md.car.sales.security;

import md.car.sales.repository.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private UserPrincipalDetailsService userPrincipalDetailsService;
    private UserRepository userRepository;

    public SecurityConfiguration(UserPrincipalDetailsService userPrincipalDetailsService, UserRepository userRepository) {
        this.userPrincipalDetailsService = userPrincipalDetailsService;
        this.userRepository = userRepository;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                // remove csrf and state in session because in jwt we do not need them
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                // add jwt filters (1. authentication, 2. authorization)
                .addFilter(new JwtAuthenticationFilter(authenticationManager()))
                .addFilter(new JwtAuthorizationFilter(authenticationManager(), this.userRepository))
                .authorizeRequests()
                // configure access rules
                .antMatchers(HttpMethod.POST, "/login").permitAll()
                .antMatchers(HttpMethod.POST, "/sign-up").permitAll()
                .antMatchers(HttpMethod.POST, "/countries").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/brands").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/comforts").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/securities").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/caches").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/caches/all").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/contact").permitAll()
                .antMatchers(HttpMethod.GET, "/ads").permitAll()
                .antMatchers(HttpMethod.GET, "/ad/**").permitAll()
                .antMatchers(HttpMethod.GET, "/activate/**").permitAll()
                .antMatchers(HttpMethod.GET, "/currencies").permitAll()
                .antMatchers(HttpMethod.GET, "/brands").permitAll()
                .antMatchers(HttpMethod.GET, "/fuels").permitAll()
                .antMatchers(HttpMethod.GET, "/bodyStyles").permitAll()
                .antMatchers(HttpMethod.GET, "/traction").permitAll()
                .antMatchers(HttpMethod.GET, "/transmissions").permitAll()
                .antMatchers(HttpMethod.GET, "/colors").permitAll()
                .antMatchers(HttpMethod.GET, "/comforts").permitAll()
                .antMatchers(HttpMethod.GET, "/securities").permitAll()
                .antMatchers(HttpMethod.GET, "/countries").permitAll()
                .antMatchers(HttpMethod.GET, "/server/status").permitAll()
                .antMatchers(HttpMethod.GET, "/statistics/**").permitAll()
                .antMatchers(HttpMethod.GET, "/ads/**/views").permitAll()

                .anyRequest().authenticated()

//                .antMatchers("/api/public/management/*").hasRole("MANAGER")
//                .antMatchers("/admin/users").hasRole("ADMIN")
        ;
    }

    @Bean
    DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        daoAuthenticationProvider.setUserDetailsService(this.userPrincipalDetailsService);

        return daoAuthenticationProvider;
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}