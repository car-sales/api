package md.car.sales.filter;

import md.car.sales.converter.*;
import md.car.sales.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FilterOptions {

    private int negotiable = 0;
    private int priceStart = 0;
    private int priceEnd = 0;
    private int mileageStart = 0;
    private int mileageEnd = 0;
    private int yearStart = 0;
    private int yearEnd = 0;
    private double capacityStart = 0;
    private double capacityEnd = 0;
    private int horsePowerStart = 0;
    private int horsePowerEnd = 0;
    private List<DealType> dealTypes;
    private List<Integer> doorsNumber;
    private Currency currency = Currency.EUR;
    private List<String> models;
    private List<String> brands;
    private List<String> countries;
    private List<String> regions;
    private List<String> securities;
    private List<String> comforts;
    private List<Fuel> fuels;
    private List<State> states;
    private List<Color> exteriorColors;
    private List<Color> interiorColors;
    private List<BodyStyle> bodyStyles;
    private List<Transmission> transmissions;
    private List<Traction> traction;
    private List<SteerWheelSide> steerWheelSides;

    private Map<String, Integer> prices;

    public void setNegotiable(boolean negotiable) {
        this.negotiable = (negotiable ? 1 : 0);
    }

    public void setPriceStart(int priceStart) {
        this.priceStart = priceStart;
    }

    public void setPriceEnd(int priceEnd) {
        this.priceEnd = priceEnd;
    }

    public void setMileageStart(int mileageStart) {
        this.mileageStart = mileageStart;
    }

    public void setMileageEnd(int mileageEnd) {
        this.mileageEnd = mileageEnd;
    }

    public void setYearStart(int yearStart) {
        this.yearStart = yearStart;
    }

    public void setYearEnd(int yearEnd) {
        this.yearEnd = yearEnd;
    }

    public void setDoorsNumber(List<Integer> doorsNumber) {
        this.doorsNumber = doorsNumber;
    }

    public void setCapacityStart(double capacityStart) {
        this.capacityStart = capacityStart;
    }

    public void setCapacityEnd(double capacityEnd) {
        this.capacityEnd = capacityEnd;
    }

    public void setHorsePowerStart(int horsePowerStart) {
        this.horsePowerStart = horsePowerStart;
    }

    public void setHorsePowerEnd(int horsePowerEnd) {
        this.horsePowerEnd = horsePowerEnd;
    }

    public void setDealTypes(List<String> dealTypes) {
        this.dealTypes = dealTypes.stream().map(c -> DealType.valueOf(c.toUpperCase())).collect(Collectors.toList());
    }

    public void setCurrency(String currency) {
        this.currency = Currency.valueOf(currency.toUpperCase());
    }

    public void setModels(List<String> models) {
        this.models = models;
    }

    public void setBrands(List<String> brands) {
        this.brands = brands;

    }

    public void setCountries(List<String> countries) {
        this.countries = countries;

    }

    public void setRegions(List<String> regions) {
        this.regions = regions;
    }

    public void setSecurities(List<String> securities) {
        this.securities = securities;
        this.securities.sort(String::compareToIgnoreCase);
    }

    public void setComforts(List<String> comforts) {
        this.comforts = comforts;
        this.comforts.sort(String::compareToIgnoreCase);
    }

    public void setFuels(List<String> fuels) {
        this.fuels = fuels.stream().map(c -> Fuel.valueOf(c.toUpperCase())).collect(Collectors.toList());
    }

    public void setStates(List<String> states) {
        this.states = states.stream().map(c -> State.valueOf(c.toUpperCase())).collect(Collectors.toList());
    }

    public void setExteriorColors(List<String> exteriorColors) {
        this.exteriorColors = exteriorColors.stream().map(c -> Color.valueOf(c.toUpperCase())).collect(Collectors.toList());
    }

    public void setInteriorColors(List<String> interiorColors) {
        this.interiorColors = interiorColors.stream().map(c -> Color.valueOf(c.toUpperCase())).collect(Collectors.toList());
    }

    public void setBodyStyles(List<String> bodyStyles) {
        this.bodyStyles = bodyStyles.stream().map(c -> BodyStyle.valueOf(c.toUpperCase())).collect(Collectors.toList());
    }

    public void setTransmissions(List<String> transmissions) {
        this.transmissions = transmissions.stream().map(c -> Transmission.valueOf(c.toUpperCase())).collect(Collectors.toList());
    }

    public void setTraction(List<String> traction) {
        for (String t : traction)
            switch (t) {
                case "4x2":
                    traction.set(traction.indexOf("4x2"), "AWD_TWO_WHEEL_DRIVE");
                    break;
                case "4x4":
                    traction.set(traction.indexOf("4x4"), "AWD_FOUR_WHEEL_DRIVE");
                    break;
            }
        this.traction = traction.stream().map(c -> Traction.valueOf(c.toUpperCase())).collect(Collectors.toList());
    }

    public void setSteerWheelSides(List<String> steerWheelSides) {
        this.steerWheelSides = steerWheelSides.stream().map(c -> SteerWheelSide.valueOf(c.toUpperCase())).collect(Collectors.toList());
    }

    public void setPrices(Map<String, Integer> prices) {
        this.prices = prices;
    }


    public int isNegotiable() {
        return negotiable;
    }

    public int getPriceStart() {
        return priceStart;
    }

    public int getPriceEnd() {
        return priceEnd;
    }

    public int getMileageStart() {
        return mileageStart;
    }

    public int getMileageEnd() {
        return mileageEnd;
    }

    public int getYearStart() {
        return yearStart;
    }

    public int getYearEnd() {
        return yearEnd;
    }

    public double getCapacityStart() {
        return capacityStart;
    }

    public double getCapacityEnd() {
        return capacityEnd;
    }

    public int getHorsePowerStart() {
        return horsePowerStart;
    }

    public int getHorsePowerEnd() {
        return horsePowerEnd;
    }

    public List<Integer> getDealTypes() {
        return (this.dealTypes == null ? new ArrayList<>() : this.dealTypes.stream().map(new DealTypeConverter()::convertToDatabaseColumn).collect(Collectors.toList()));
    }

    public List<Integer> getDoorsNumber() {
        return (this.doorsNumber == null ? new ArrayList<>() : this.doorsNumber);
    }


    public List<String> getModels() {
        return (this.models == null ? new ArrayList<>() : this.models);
    }

    public Currency getCurrency() {
        return currency;
    }

    public List<String> getBrands() {
        return brands;
    }

    public List<String> getCountries() {
        return countries;
    }

    public List<String> getRegions() {
        return (this.regions == null ? new ArrayList<>() : this.regions);
    }

    public String getSecurities() {
        return this.computeFeatures(this.securities);
    }

    public String getComforts() {
        return this.computeFeatures(this.comforts);
    }

    public List<Integer> getFuels() {
        return (this.fuels == null ? new ArrayList<>() : this.fuels.stream().map(new FuelConverter()::convertToDatabaseColumn).collect(Collectors.toList()));
    }

    public List<Integer> getStates() {
        return (this.states == null ? new ArrayList<>() : this.states.stream().map(new StateConverter()::convertToDatabaseColumn).collect(Collectors.toList()));
    }

    public List<Integer> getExteriorColors() {
        return (this.exteriorColors == null ? new ArrayList<>() : this.exteriorColors.stream().map(new ColorConverter()::convertToDatabaseColumn).collect(Collectors.toList()));
    }

    public List<Integer> getInteriorColors() {
        return (this.interiorColors == null ? new ArrayList<>() : this.interiorColors.stream().map(new ColorConverter()::convertToDatabaseColumn).collect(Collectors.toList()));
    }

    public List<Integer> getBodyStyles() {
        return (this.bodyStyles == null ? new ArrayList<>() : this.bodyStyles.stream().map(new BodyStyleConverter()::convertToDatabaseColumn).collect(Collectors.toList()));
    }

    public List<Integer> getTransmissions() {
        return (this.transmissions == null ? new ArrayList<>() : this.transmissions.stream().map(new TransmissionConverter()::convertToDatabaseColumn).collect(Collectors.toList()));
    }

    public List<Integer> getTraction() {
        return (this.traction == null ? new ArrayList<>() : this.traction.stream().map(new TractionConverter()::convertToDatabaseColumn).collect(Collectors.toList()));
    }

    public List<Integer> getSteerWheelSides() {
        return (this.steerWheelSides == null ? new ArrayList<>() : this.steerWheelSides.stream().map(new SteerWheelSideConverter()::convertToDatabaseColumn).collect(Collectors.toList()));
    }

    public Map<String, Integer> getPrices() {
        return prices;
    }

    /**
     * @param features which ar comforts or securities
     * @return a String of features for sql query
     */
    private String computeFeatures(List<String> features) {
        StringBuilder sb = new StringBuilder();
        try {
            for (String feature : features) {
                sb.append("%").append(feature);
            }
            sb.append("%");
        } catch (Exception e) {
            sb.append("%");
        }
        return sb.toString();
    }
}
