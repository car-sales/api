package md.car.sales.filter;

import lombok.NoArgsConstructor;
import md.car.sales.currency.DailyCurrency;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@NoArgsConstructor
@Component
public class PriceCalculator {

    private List<String> currenciesName = new ArrayList<>(Arrays.asList("EUR", "MDL", "RON", "USD"));

    @Autowired
    private DailyCurrency dailyCurrency;

    private Map<String, Integer> emptyPrices = Stream.of(new Object[][]{
            {"priceStartInEUR", 0},
            {"priceStartInUSD", 0},
            {"priceStartInRON", 0},
            {"priceStartInMDL", 0},
            {"priceEndInEUR", 100 * 100 * 1000},
            {"priceEndInUSD", 100 * 100 * 1000},
            {"priceEndInRON", 100 * 100 * 1000},
            {"priceEndInMDL", 100 * 100 * 2000}
    }).collect(Collectors.toMap(data -> (String) data[0], data -> (Integer) data[1]));

    public Map<String, Integer> getPrices(FilterOptions filterOptions) {

        if (filterOptions.getPriceStart() == 0 && filterOptions.getPriceEnd() == 0)
            return this.emptyPrices;
        Map<String, Integer> pricesMap = new HashMap<>();

        return fillPricesMap(filterOptions.getCurrency().toString(),
                filterOptions.getPriceStart(), filterOptions.getPriceEnd(), pricesMap);
    }

    private Map<String, Integer> fillPricesMap(String currency, int priceStart, int priceEnd, Map<String, Integer> pricesMap) {

        for (String currencyName : currenciesName) {
            if (currencyName.equals(currency)) {
                pricesMap.put("priceStartIn" + currency, priceStart);
                pricesMap.put("priceEndIn" + currency, priceEnd);
            } else {

                pricesMap.put("priceStartIn" + currencyName, ((int) Math.round(priceStart * dailyCurrency.getConvertedCurrency(currency + "to" + currencyName))));
                pricesMap.put("priceEndIn" + currencyName, ((int) Math.round(priceEnd * dailyCurrency.getConvertedCurrency(currency + "to" + currencyName))));
            }
        }

        return pricesMap;
    }

}
