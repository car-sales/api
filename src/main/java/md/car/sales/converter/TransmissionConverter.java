package md.car.sales.converter;

import md.car.sales.model.Transmission;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static md.car.sales.model.Transmission.*;

@Converter
public class TransmissionConverter implements AttributeConverter<Transmission, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Transmission attribute) {
        switch (attribute) {
            case AUTOMATIC:
                return 1;
            case MANUAL:
                return 2;
            case AUTO_MANUAL:
                return 3;
            case CVT:
                return 4;
            case OTHER:
                return 5;
            default:
                throw new IllegalArgumentException("Unknown" + attribute);
        }
    }

    @Override
    public Transmission convertToEntityAttribute(Integer dbData) {
        switch (dbData) {
            case 1:
                return AUTOMATIC;
            case 2:
                return MANUAL;
            case 3:
                return AUTO_MANUAL;
            case 4:
                return CVT;
            case 5:
                return OTHER;
            default:
                throw new IllegalArgumentException("Unknown" + dbData);
        }
    }
}
