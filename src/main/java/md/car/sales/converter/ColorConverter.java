package md.car.sales.converter;

import md.car.sales.model.Color;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static md.car.sales.model.Color.*;

@Converter
public class ColorConverter implements AttributeConverter<Color, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Color attribute) {
        switch (attribute) {
            case BEIGE:
                return 1;
            case BLACK:
                return 2;
            case BLUE:
                return 3;
            case BROWN:
                return 4;
            case GOLD:
                return 5;
            case GRAY:
                return 6;
            case GREEN:
                return 7;
            case ORANGE:
                return 8;
            case PINK:
                return 9;
            case PURPLE:
                return 10;
            case RED:
                return 11;
            case SILVER:
                return 12;
            case WHITE:
                return 13;
            case YELLOW:
                return 14;
            case OTHER:
                return 15;

            default:
                throw new IllegalArgumentException("Unknown" + attribute);
        }
    }

    @Override
    public Color convertToEntityAttribute(Integer dbData) {
        switch (dbData) {
            case 1:
                return BEIGE;
            case 2:
                return BLACK;
            case 3:
                return BLUE;
            case 4:
                return BROWN;
            case 5:
                return GOLD;
            case 6:
                return GRAY;
            case 7:
                return GREEN;
            case 8:
                return ORANGE;
            case 9:
                return PINK;
            case 10:
                return PURPLE;
            case 11:
                return RED;
            case 12:
                return SILVER;
            case 13:
                return WHITE;
            case 14:
                return YELLOW;
            case 15:
                return OTHER;

            default:
                throw new IllegalArgumentException("Unknown" + dbData);
        }
    }
}
