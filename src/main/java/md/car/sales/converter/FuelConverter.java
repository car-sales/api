package md.car.sales.converter;

import md.car.sales.model.Fuel;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static md.car.sales.model.Fuel.*;

@Converter
public class FuelConverter implements AttributeConverter<Fuel, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Fuel attribute) {
        switch (attribute) {
            case HYBRID:
                return 1;
            case PLUG_IN_HYBRID:
                return 2;
            case GASOLINE:
                return 3;
            case DIESEL:
                return 4;
            case ELECTRIC:
                return 5;
            case GAS:
                return 6;
            default:
                throw new IllegalArgumentException("Unknown" + attribute);
        }
    }

    @Override
    public Fuel convertToEntityAttribute(Integer dbData) {
        switch (dbData) {
            case 1:
                return HYBRID;
            case 2:
                return PLUG_IN_HYBRID;
            case 3:
                return GASOLINE;
            case 4:
                return DIESEL;
            case 5:
                return ELECTRIC;
            case 6:
                return GAS;
            default:
                throw new IllegalArgumentException("Unknown" + dbData);
        }
    }
}
