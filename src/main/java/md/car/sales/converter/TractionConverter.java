package md.car.sales.converter;

import md.car.sales.model.Traction;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static md.car.sales.model.Traction.*;

@Converter
public class TractionConverter implements AttributeConverter<Traction, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Traction attribute) {
        switch (attribute) {
            case AWD:
                return 1;
            case RWD:
                return 2;
            case FWD:
                return 3;
            case AWD_TWO_WHEEL_DRIVE:
                return 4;
            case AWD_FOUR_WHEEL_DRIVE:
                return 5;
            default:
                throw new IllegalArgumentException("Unknown" + attribute);
        }
    }

    @Override
    public Traction convertToEntityAttribute(Integer dbData) {
        switch (dbData) {
            case 1:
                return AWD;
            case 2:
                return RWD;
            case 3:
                return FWD;
            case 4:
                return AWD_TWO_WHEEL_DRIVE;
            case 5:
                return AWD_FOUR_WHEEL_DRIVE;
            default:
                throw new IllegalArgumentException("Unknown" + dbData);
        }
    }
}
