package md.car.sales.converter;

import md.car.sales.model.DealType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static md.car.sales.model.DealType.*;

@Converter
public class DealTypeConverter implements AttributeConverter<DealType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(DealType attribute) {
        switch (attribute) {
            case SALE:
                return 1;
            case BUY:
                return 2;
            case CHANGE:
                return 3;
            default:
                throw new IllegalArgumentException("Unknown" + attribute);
        }
    }

    @Override
    public DealType convertToEntityAttribute(Integer dbData) {
        switch (dbData) {
            case 1:
                return SALE;
            case 2:
                return BUY;
            case 3:
                return CHANGE;
            default:
                throw new IllegalArgumentException("Unknown" + dbData);
        }
    }
}