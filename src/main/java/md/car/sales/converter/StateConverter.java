package md.car.sales.converter;

import md.car.sales.model.State;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static md.car.sales.model.State.*;

@Converter
public class StateConverter implements AttributeConverter<State, Integer> {

    @Override
    public Integer convertToDatabaseColumn(State attribute) {
        switch (attribute) {
            case NEW:
                return 1;
            case USED:
                return 2;
            case NEEDS_REPAIR:
                return 3;
            default:
                throw new IllegalArgumentException("Unknown" + attribute);
        }
    }

    @Override
    public State convertToEntityAttribute(Integer dbData) {
        switch (dbData) {
            case 1:
                return NEW;
            case 2:
                return USED;
            case 3:
                return NEEDS_REPAIR;
            default:
                throw new IllegalArgumentException("Unknown" + dbData);
        }
    }
}

