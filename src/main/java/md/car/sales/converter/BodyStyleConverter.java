package md.car.sales.converter;

import md.car.sales.model.BodyStyle;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static md.car.sales.model.BodyStyle.*;

@Converter
public class BodyStyleConverter implements AttributeConverter<BodyStyle, Integer> {

    @Override
    public Integer convertToDatabaseColumn(BodyStyle attribute) {
        switch (attribute) {
            case CARGO_VAN:
                return 1;
            case CONVERTIBLE:
                return 2;
            case COUPE:
                return 3;
            case PICKUP:
                return 4;
            case HATCHBACK:
                return 5;
            case MINIVAN:
                return 6;
            case PASSENGER_VAN:
                return 7;
            case SUV:
                return 8;
            case SEDAN:
                return 9;
            case WAGON:
                return 10;

            default:
                throw new IllegalArgumentException("Unknown" + attribute);
        }
    }

    @Override
    public BodyStyle convertToEntityAttribute(Integer dbData) {
        switch (dbData) {
            case 1:
                return CARGO_VAN;
            case 2:
                return CONVERTIBLE;
            case 3:
                return COUPE;
            case 4:
                return PICKUP;
            case 5:
                return HATCHBACK;
            case 6:
                return MINIVAN;
            case 7:
                return PASSENGER_VAN;
            case 8:
                return SUV;
            case 9:
                return SEDAN;
            case 10:
                return WAGON;

            default:
                throw new IllegalArgumentException("Unknown" + dbData);
        }
    }
}