package md.car.sales.converter;

import md.car.sales.model.SteerWheelSide;

import javax.persistence.AttributeConverter;
import javax.persistence.Convert;

import static md.car.sales.model.SteerWheelSide.LEFT;
import static md.car.sales.model.SteerWheelSide.RIGHT;

@Convert
public class SteerWheelSideConverter implements AttributeConverter<SteerWheelSide,Integer> {

    @Override
    public Integer convertToDatabaseColumn(SteerWheelSide attribute) {
        switch (attribute) {
            case LEFT:
                return 1;
            case RIGHT:
                return 2;
            default:
                throw new IllegalArgumentException("Unknown" + attribute);
        }
    }

    @Override
    public SteerWheelSide convertToEntityAttribute(Integer dbData) {
        switch (dbData) {
            case 1:
                return LEFT;
            case 2:
                return RIGHT;
            default:
                throw new IllegalArgumentException("Unknown" + dbData);
        }
    }
}
