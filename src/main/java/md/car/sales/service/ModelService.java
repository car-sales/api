package md.car.sales.service;

import md.car.sales.model.Brand;
import md.car.sales.model.Model;
import md.car.sales.projection.ModelView;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ModelService {

    void addModels(List<Model> models);

    void addModelsByBrand(Brand brand);

//    List<Model> getModelsByBrand(String brandName);

    ModelView getModelById(long id);

    List<ModelView> getModels();
}
