package md.car.sales.service.serviceImpl;

import md.car.sales.service.StatisticService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Service
public class StatisticServiceImpl implements StatisticService {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    @Override
    public void addViewToAd(long adId) {
        em.joinTransaction();

        String query = "INSERT INTO ad_view(ad_id) VALUES(?)";
        em.createNativeQuery(query)
                .setParameter(1, adId)
                .executeUpdate();

        try {
            this.updateStatistics();
            int viewsCount = Integer.parseInt(em.createNativeQuery("SELECT s.views FROM statistics s WHERE s.date = '" + this.getCurrentDate() + "'")
                    .getResultList().get(0).toString());
            String addViewToStatistics = updateQuery("views", viewsCount + 1);
            em.createNativeQuery(addViewToStatistics)
                    .executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

        em.close();
    }

    @CacheEvict(value = "addedAdsCount", allEntries = true)
    @Transactional
    @Override
    public void incAddedAds() {

        this.updateStatistics();

        em.joinTransaction();

        int adsCount;

        String adsCountByDate = selectAdsCountQuery();

        adsCount = Integer.parseInt(em.createNativeQuery(adsCountByDate)
                .getResultList().get(0).toString());

        String query = updateQuery("ads_added", adsCount + 1);
        em.createNativeQuery(query)
                .executeUpdate();

        em.close();
    }

    @Transactional
    @Override
    public int getAdViews(long id) {

        em.joinTransaction();

        int viewsCount;

        String query = "SELECT COUNT(a.id) FROM ad_view a WHERE a.ad_id = " + id;

        viewsCount = Integer.parseInt(em.createNativeQuery(query)
                .getResultList().get(0).toString());

        em.close();
        return viewsCount;
    }

    @Transactional
    @Override
    public int getTodayAdViews(long id) {
        em.joinTransaction();

        int todayViewsCount;

        String query = "SELECT COUNT(a.id) FROM ad_view a WHERE a.ad_id = " + id + " AND a.date > '" + this.getCurrentDate() + "' " +
                "AND a.date < '" + this.getNextDate() + "'";

        todayViewsCount = Integer.parseInt(em.createNativeQuery(query).getResultList().get(0).toString());

        em.close();
        return todayViewsCount;
    }

    @Transactional
    @Override
    public int getAddedAdsCount() {
        try {
            this.updateStatistics();
        } catch (RuntimeException e) {
            System.out.println("Error on inserting new date in statistics.");
        }
        em.joinTransaction();

        int count;

        String query = selectAdsCountQuery();

        count = Integer.parseInt(em.createNativeQuery(query)
                .getResultList().get(0).toString());

        em.close();
        return count;
    }

    @Transactional()
    protected void updateStatistics() {

        em.joinTransaction();

        String query = "INSERT INTO statistics(views) SELECT 0 WHERE NOT EXISTS ( SELECT id FROM statistics WHERE date = '" + this.getCurrentDate() + "')";
        em.createNativeQuery(query)
                .executeUpdate();

        em.close();

    }

    private String updateQuery(String column, int value) {
        return "UPDATE statistics SET " + column + "=" + value + " WHERE date='" + this.getCurrentDate() + "'";
    }

    private String selectAdsCountQuery() {
        return "SELECT s.ads_added FROM statistics s WHERE s.date = '" + this.getCurrentDate() + "'";
    }

    private String getNextDate() {
        try {
            final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            final Date date = format.parse(this.getCurrentDate());
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            return format.format(calendar.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getCurrentDate() {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final Date date = new Date();

        return dateFormat.format(date);

    }
}


