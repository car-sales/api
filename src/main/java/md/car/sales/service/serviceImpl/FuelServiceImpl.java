package md.car.sales.service.serviceImpl;

import md.car.sales.model.Fuel;
import md.car.sales.service.FuelService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class FuelServiceImpl implements FuelService {

    @Override
    public List<String> getFuels() {
        return Stream.of(Fuel.values())
                .map(Fuel::getName)
                .collect(Collectors.toList());
    }
}
