package md.car.sales.service.serviceImpl;

import md.car.sales.model.DealType;
import md.car.sales.service.DealTypeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DealTypeServiceImpl implements DealTypeService {
    @Override
    public List<String> getDealTypes() {
        return Stream.of(DealType.values())
                .map(DealType::getName)
                .collect(Collectors.toList());
    }
}
