package md.car.sales.service.serviceImpl;

import md.car.sales.dto.CountryDTO;
import md.car.sales.mapper.Mapper;
import md.car.sales.model.Country;
import md.car.sales.model.Region;
import md.car.sales.repository.CountryRepository;
import md.car.sales.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CountryServiceImpl implements CountryService {

    private CountryRepository countryRepo;

    @Autowired
    private Mapper mapper;

    public CountryServiceImpl(CountryRepository countryRepo) {
        this.countryRepo = countryRepo;
    }

    @Cacheable(value = "getCountries", unless = "#result==null")
    @Override
    public List<CountryDTO> getCountries() {

        return countryRepo.findAll().stream().map(mapper::toCountryDto).collect(Collectors.toList());
    }

    @Override
    public Country getCountryById(long id) {
        return countryRepo.findById(id);
    }

    @Override
    public Country getCountryByName(String countryName) {
        return countryRepo.findAllByCountryName(countryName);
    }

    @CacheEvict(value = "getCountries", allEntries = true)
    @Override
    public void addCountries(List<Country> countries) {

        List<Country> countryList = new ArrayList<>();

        for (Country country : countries) {
            Country country2 = new Country(country.getCountryName());
            for (Region region : country.getRegions()) {
                country2.addRegions(new Region(region.getRegionName()));
            }
            countryList.add(country2);
        }
        countryRepo.saveAll(countryList);
    }
}
