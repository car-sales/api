package md.car.sales.service.serviceImpl;

import md.car.sales.service.CacheService;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class CacheServiceImpl implements CacheService {

    private CacheManager cacheManager;

    public CacheServiceImpl(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    @Override
    public void evictAllCaches() {
        cacheManager.getCacheNames()
                .forEach(cacheName -> cacheManager.getCache(cacheName).clear());
    }

    @Override
    public void evictCacheByNameAndKey(String cacheName, String key) {
        Objects.requireNonNull(cacheManager.getCache(cacheName)).evictIfPresent(key);
    }

    @Override
    public void putCacheByKey(String name, String key, Object object) {
        Objects.requireNonNull(cacheManager.getCache(name)).putIfAbsent(key, object);
    }


}
