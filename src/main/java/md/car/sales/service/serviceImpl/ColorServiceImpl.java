package md.car.sales.service.serviceImpl;

import md.car.sales.model.Color;
import md.car.sales.service.ColorService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ColorServiceImpl implements ColorService {
    @Override
    public List<String> getColors() {
        return Stream.of(Color.values())
                .map(Color::getName)
                .collect(Collectors.toList());
    }
}
