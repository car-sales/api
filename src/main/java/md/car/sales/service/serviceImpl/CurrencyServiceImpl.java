package md.car.sales.service.serviceImpl;

import md.car.sales.model.Currency;
import md.car.sales.service.CurrencyService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CurrencyServiceImpl implements CurrencyService {
    @Override
    public List<String> getCurrencies() {
        List<String> currencies = Stream.of(Currency.values())
                .map(Currency::getName)
                .collect(Collectors.toList());
        currencies.remove("empty");

        return currencies;
    }
}
