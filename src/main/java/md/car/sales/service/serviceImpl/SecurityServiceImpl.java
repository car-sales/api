package md.car.sales.service.serviceImpl;

import md.car.sales.model.Security;
import md.car.sales.repository.SecurityRepository;
import md.car.sales.service.SecurityService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SecurityServiceImpl implements SecurityService {

    private final SecurityRepository securityRepo;

    public SecurityServiceImpl(SecurityRepository securityRepo) {
        this.securityRepo = securityRepo;
    }

    @Override
    public void addSecurity(Security security) {
        securityRepo.save(security);
    }

    @Cacheable("getSecurities")
    @Override
    public List<Security> addSecurities(List<Security> securities) {
        this.clearSecuritiesCache();
        try {
            securityRepo.saveAll(securities);
        } catch (Exception e) {
            return null;
        }

        return securityRepo.findAll();
    }

    @Cacheable("getSecurities")
    @Override
    public List<Security> getSecurities() {
        return securityRepo.findAll();
    }

    @CacheEvict(value = "getSecurities", allEntries = true)
    public void clearSecuritiesCache() {

    }
}
