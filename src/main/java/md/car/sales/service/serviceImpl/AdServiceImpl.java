package md.car.sales.service.serviceImpl;

import md.car.sales.dto.AdPreviewDTO;
import md.car.sales.dto.AdViewDTO;
import md.car.sales.dto.AdsPage;
import md.car.sales.filter.FilterOptions;
import md.car.sales.filter.PriceCalculator;
import md.car.sales.image.ImageStore;
import md.car.sales.mapper.Mapper;
import md.car.sales.model.*;
import md.car.sales.repository.AdRepository;
import md.car.sales.repository.CarRepository;
import md.car.sales.repository.EngineRepository;
import md.car.sales.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AdServiceImpl implements AdService {

    private final AdRepository adRepo;
    private final CarRepository carRepo;
    private final EngineRepository engineRepo;
    private final UserService userService;
    private final BrandService brandService;
    private final CountryService countryService;
    private final ImageStore imageStore;

    private final StatisticService statisticService;

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private Mapper mapper;

    @Autowired
    PriceCalculator priceCalculator;

    private final String[][] deleteData = {{"user_liked_ads", "ad_id", ""},
            {"ad", "ad.id", ""},
            {"car_comfort", "car_id", ""},
            {"car_security", "car_id", ""},
            {"car", "car.id", ""},
            {"engine", "engine.id", ""},
            {"ad_images_320x240", "ad_id", ""},
            {"ad_images_1280x960", "ad_id", ""}
    };

    public AdServiceImpl(AdRepository adRepo, CarRepository carRepo,
                         EngineRepository engineRepo, UserService userService,
                         StatisticService statisticService,
                         BrandService brandService, CountryService countryService,
                         ImageStore imageStore) {

        this.adRepo = adRepo;
        this.carRepo = carRepo;
        this.engineRepo = engineRepo;
        this.userService = userService;
        this.statisticService = statisticService;
        this.brandService = brandService;
        this.countryService = countryService;
        this.imageStore = imageStore;
    }

    @Override
    public AdsPage getAdsByCriteria(Pageable pageable, FilterOptions filterOptions) {

        List<String> models = filterOptions.getModels();
        List<String> regions = filterOptions.getRegions();

        if (filterOptions.getBrands() != null) {
            for (String brandName : filterOptions.getBrands()) {
                models.addAll(brandService.getBrandByBrandName(brandName).getModels()
                        .stream().map(Model::getModelName).collect(Collectors.toList()));
            }
            filterOptions.setModels(models);
        }

        if (filterOptions.getCountries() != null) {
            for (String countryName : filterOptions.getCountries()) {
                regions.addAll(countryService.getCountryByName(countryName).getRegions()
                        .stream().map(Region::getRegionName).collect(Collectors.toList()));

            }
            filterOptions.setRegions(regions);
        }

        Map<String, Integer> prices = priceCalculator.getPrices(filterOptions);
        filterOptions.setPrices(prices);

        Page<Object[]> ads = adRepo.findAll(filterOptions, pageable);
        List<AdPreviewDTO> adsDTO = ads.getContent().stream().map(mapper::toAdPreviewDto).collect(Collectors.toList());
        AdsPage page = new AdsPage();

        page.setContent(adsDTO);
        page.setNumber(ads.getNumber());
        page.setPageSize(ads.getSize());
        page.setNumOfElements((int) ads.getTotalElements());
        page.setTotalPages(ads.getTotalPages());
        page.setAdsAddedToday(statisticService.getAddedAdsCount());

        return page;
    }

    @Cacheable(value = "adById", key = "#id", unless = "#result==null")
    @Override
    public AdViewDTO getAdById(long id) {
        statisticService.addViewToAd(id);

        try {
            return mapper.toAdViewDTO(adRepo.findAllById(id));
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Object> getAdBigImages(long id) {
        return imageStore.getFullSizeImages(id);
    }

    @Async("threadPoolTaskExecutor")
    @Override
    public void incAdViews(long id) {
        statisticService.addViewToAd(id);
    }

    @Override
    public Map<String, Integer> getAdViews(long id) {
        this.incAdViews(id);

        Map<String, Integer> adViews = new HashMap<>();
        adViews.put("views", statisticService.getAdViews(id));
        adViews.put("todayViews", statisticService.getTodayAdViews(id));

        return adViews;
    }

    @CacheEvict(value = "adById", key = "#id")
    @Transactional
    @Override
    public void removeAdById(String id) {

        Ad ad = em.find(Ad.class, Long.parseLong(id));

        String carId = String.valueOf(ad.getCar().getId());
        String engineId = String.valueOf(ad.getCar().getEngine().getId());

        deleteData[0][2] = deleteData[1][2] = deleteData[6][2] = deleteData[7][2] = id;
        deleteData[2][2] = deleteData[3][2] = deleteData[4][2] = carId;
        deleteData[5][2] = engineId;

        executeTransaction();
    }

    @Override
    public void saveAd(List<MultipartFile> files, Ad ad, String token) {

        try {
            User user = userService.getFullCurrentUser();

            Engine engine = engineRepo.save(ad.getCar().getEngine());
            Car car = ad.getCar();
            car.setEngine(engine);

            car = carRepo.save(car);

            ad.setCar(car);
            ad.setUser(user);

            long adId = adRepo.save(ad).getId();

            saveImages(files, adId);

            statisticService.incAddedAds();

        } catch (NullPointerException ignored) {

        }
    }

    private void saveImages(List<MultipartFile> files, long adId) {
        try {
            imageStore.saveCarImages(files, adId);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void executeTransaction() {
        em.joinTransaction();

        StringBuilder sb = new StringBuilder();

        for (String[] row : deleteData) {
            sb.append("DELETE FROM ")
                    .append(row[0])
                    .append(" WHERE ")
                    .append(row[1])
                    .append("=")
                    .append(row[2]);

            em.createNativeQuery(sb.toString())
                    .executeUpdate();

            sb.setLength(0);
        }

        em.close();
    }

}
