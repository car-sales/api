package md.car.sales.service.serviceImpl;

import md.car.sales.model.Brand;
import md.car.sales.model.Model;
import md.car.sales.projection.ModelView;
import md.car.sales.repository.BrandRepository;
import md.car.sales.repository.ModelRepository;
import md.car.sales.service.ModelService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ModelServiceImpl implements ModelService {

    private final BrandRepository brandRepo;
    private final ModelRepository modelRepo;

    public ModelServiceImpl(BrandRepository brandRepo, ModelRepository modelRepo) {
        this.brandRepo = brandRepo;
        this.modelRepo = modelRepo;
    }

    @Override
    public void addModels(List<Model> models) {
        modelRepo.saveAll(models);
    }

    @Override
    public void addModelsByBrand(Brand brand) {
        Brand brand1 = brandRepo.findAllByBrandName(brand.getBrandName());
        Brand brand2 = brandRepo.save(brand1);

        List<Model> brandModels = new ArrayList<>();
        for (Model model : brand.getModels()) {
            model.setBrand(brand2);
            brandModels.add(model);
        }

        addModels(brandModels);
    }

    @Override
    public ModelView getModelById(long id) {
        return modelRepo.findAllById(id);
    }

    @Override
    public List<ModelView> getModels() {
        return modelRepo.findAllBy();
    }
}
