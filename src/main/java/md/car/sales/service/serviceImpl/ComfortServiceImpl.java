package md.car.sales.service.serviceImpl;

import md.car.sales.model.Comfort;
import md.car.sales.repository.ComfortRepository;
import md.car.sales.service.ComfortService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComfortServiceImpl implements ComfortService {

    private final ComfortRepository comfortRepo;

    public ComfortServiceImpl(ComfortRepository comfortRepo) {
        this.comfortRepo = comfortRepo;
    }

    @Override
    public void addComfort(Comfort comfort) {
        comfortRepo.save(comfort);
    }

    @Cacheable(value = "getComforts", unless = "#result==null")
    @Override
    public List<Comfort> addComforts(List<Comfort> comfort) {
        this.clearComfortsCache();
        try {
            comfortRepo.saveAll(comfort);
        } catch (Exception e) {
            return null;
        }
        return comfortRepo.findAll();
    }

    @Cacheable(value = "getComforts", unless = "#result==null")
    @Override
    public List<Comfort> getComforts() {
        return comfortRepo.findAll();
    }

    @CacheEvict(value = "getComforts", allEntries = true)
    public void clearComfortsCache() {

    }
}
