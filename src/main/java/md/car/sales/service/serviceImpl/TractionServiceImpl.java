package md.car.sales.service.serviceImpl;

import md.car.sales.model.Traction;
import md.car.sales.service.TractionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TractionServiceImpl implements TractionService {
    @Override
    public List<String> getTractions() {
        return Stream.of(Traction.values())
                .map(Traction::getName)
                .collect(Collectors.toList());
    }
}
