package md.car.sales.service.serviceImpl;

import md.car.sales.model.State;
import md.car.sales.service.StateService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class StateServiceImpl implements StateService {
    @Override
    public List<String> getStates() {
        return Stream.of(State.values())
                .map(State::getName)
                .collect(Collectors.toList());
    }
}
