package md.car.sales.service.serviceImpl;

import md.car.sales.model.BodyStyle;
import md.car.sales.service.BodyStyleService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class BodyStyleServiceImpl implements BodyStyleService {
    @Override
    public List<String> getBodyStyles() {
        return Stream.of(BodyStyle.values())
                .map(BodyStyle::getName)
                .collect(Collectors.toList());
    }
}
