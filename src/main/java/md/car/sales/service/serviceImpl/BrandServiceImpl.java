package md.car.sales.service.serviceImpl;

import md.car.sales.dto.BrandDTO;
import md.car.sales.mapper.Mapper;
import md.car.sales.model.Brand;
import md.car.sales.model.Model;
import md.car.sales.repository.BrandRepository;
import md.car.sales.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BrandServiceImpl implements BrandService {

    private BrandRepository brandRepo;

    @Autowired
    private Mapper mapper;

    public BrandServiceImpl(BrandRepository brandRepo) {
        this.brandRepo = brandRepo;
    }

    @Cacheable("getBrands")
    @Override
    public List<BrandDTO> getBrands() {
        return brandRepo.findAllByOrderByBrandNameAsc().stream()
                .map(mapper::toBrandDto).collect(Collectors.toList());
    }

    @Override
    public Brand getBrandByBrandName(String brandName) {
        return brandRepo.findAllByBrandName(brandName);
    }

    @Cacheable(value = "getBrands")
    @Override
    public List<BrandDTO> addBrands(List<Brand> brands) {

        this.clearBrandsCache();
        List<BrandDTO> addedBrands = new ArrayList<>();

        for (Brand brand : brands) {
            Brand brand2 = new Brand(brand.getBrandName());
            long brandId = brandRepo.save(brand2).getId();
            brand2.setId(brandId);
            for (Model model : brand.getModels()) {
                Model model1 = new Model(model.getModelName());
                model1.setBrand(brand2);
                brand2.addModels(model1);
            }
            brandRepo.save(brand2);
        }

        return this.getBrands();
    }

    @CacheEvict(value = "getBrands", allEntries = true)
    public void clearBrandsCache() {
    }
}
