package md.car.sales.service.serviceImpl;

import md.car.sales.dto.UserDTO;
import md.car.sales.dto.UserProfileDTO;
import md.car.sales.mapper.Mapper;
import md.car.sales.model.Ad;
import md.car.sales.model.User;
import md.car.sales.projection.UserProfileView;
import md.car.sales.repository.AdRepository;
import md.car.sales.repository.UserRepository;
import md.car.sales.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepo;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private AdRepository adRepo;

    @Autowired
    private Mapper mapper;

    public UserServiceImpl(UserRepository userRepo, BCryptPasswordEncoder bCryptPasswordEncoder,
                           AdRepository adRepo
    ) {
        this.userRepo = userRepo;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.adRepo = adRepo;
    }

    @Override
    public User saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return userRepo.save(user);
    }

    @Override
    public void deleteUserById(long id) {
        userRepo.deleteById(id);
    }

    @Override
    public List<UserProfileView> geUsers() {
        return userRepo.findAllBy();
    }

    @Override
    public UserProfileDTO getCurrentUser() {
        User user = userRepo.findAllByEmail(getCurrentUserEmail());

        return mapper.toUserProfileDTO(user, adRepo.findAllByUserId(user.getId()));
    }

    @Override
    public UserDTO getUserById(long id) {
        User user = userRepo.findAllById(id);

        return mapper.toUserDTO(user);
    }

    @Override
    public User getFullCurrentUser() {
        return userRepo.findByEmail(getCurrentUserEmail());
    }

    @Override
    public void addLikedAd(long id) {
        User currentUser = getFullCurrentUser();
        currentUser.addLikedAd(adRepo.findById(id));
        userRepo.save(currentUser);
    }

    @Override
    public void unlikeAd(long id) {
        User currentUser = getFullCurrentUser();
        currentUser.removeLikedAd(id);
        userRepo.save(currentUser);
    }

    @Override
    public boolean userExistsByEmail(String email) {
        return (userRepo.findByEmail(email) != null);
    }

    @Override
    public boolean userExistsByUsername(String userName) {
        return (userRepo.findByUserName(userName) != null);
    }

    private String getCurrentUserEmail() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        return auth.getPrincipal().toString();
    }

    @Override
    public void activateUserAccount(long id) {
        User inactiveUser = userRepo.findAllById(id);

        inactiveUser.setActive(1);
        userRepo.save(inactiveUser);
    }

    @Override
    public boolean isInactiveById(long id) {
        return (userRepo.findAllById(id).getActive() == 0);
    }

    @Override
    public boolean isLikedByCurrentUser(long adId) {
        long userId = 0L;
        try {
            userId = this.getCurrentUser().getId();
        } catch (Exception e) {
        }

        if (userId == 0L) return false;

        Ad ad = adRepo.findById(adId);
        User user = userRepo.findAllByIdAndLikedAdsIsContaining(userId, ad);
        return user != null;

    }

}
