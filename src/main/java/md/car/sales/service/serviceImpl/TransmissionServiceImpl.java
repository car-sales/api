package md.car.sales.service.serviceImpl;

import md.car.sales.model.Transmission;
import md.car.sales.service.TransmissionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TransmissionServiceImpl implements TransmissionService {
    @Override
    public List<String> getTransmissions() {
        return Stream.of(Transmission.values())
                .map(Transmission::getName)
                .collect(Collectors.toList());
    }
}
