package md.car.sales.service;

import md.car.sales.dto.AdViewDTO;
import md.car.sales.dto.AdsPage;
import md.car.sales.filter.FilterOptions;
import md.car.sales.model.Ad;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Service
public interface AdService {

    AdsPage getAdsByCriteria(Pageable pageable, FilterOptions filterOptions) throws IOException;

    AdViewDTO getAdById(long id);

    List<Object> getAdBigImages(long id);

    void incAdViews(long id);

    Map<String, Integer> getAdViews(long id);

    void removeAdById(String id);

    void saveAd(List<MultipartFile> files, Ad ad,String token);
}
