package md.car.sales.service;

import org.springframework.stereotype.Service;

@Service
public interface CacheService {
    void evictAllCaches();

    void evictCacheByNameAndKey(String cacheName, String key);

    void putCacheByKey(String name, String key, Object object);
}
