package md.car.sales.service;

import java.util.List;

public interface DealTypeService {
    List<String> getDealTypes();

}
