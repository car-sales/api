package md.car.sales.service;

import md.car.sales.model.Comfort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ComfortService {
    void addComfort(Comfort comfort);

    List<Comfort> addComforts(List<Comfort> comfort);

    List<Comfort> getComforts();
}
