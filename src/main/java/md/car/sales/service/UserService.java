package md.car.sales.service;

import md.car.sales.dto.UserDTO;
import md.car.sales.dto.UserProfileDTO;
import md.car.sales.model.User;
import md.car.sales.projection.UserProfileView;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    User saveUser(User user);

    void deleteUserById(long id);

    List<UserProfileView> geUsers();

    UserProfileDTO getCurrentUser();

    User getFullCurrentUser();

    UserDTO getUserById(long id);

    void addLikedAd(long id);

    void unlikeAd(long id);

    boolean userExistsByEmail(String email);

    boolean userExistsByUsername(String userName);

    void activateUserAccount(long id);

    boolean isInactiveById(long id);

    boolean isLikedByCurrentUser(long adId);

}
