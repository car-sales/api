package md.car.sales.service;

import md.car.sales.model.Security;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SecurityService {
    void addSecurity(Security security);

    List<Security> addSecurities(List<Security> securities);

    List<Security> getSecurities();
}
