package md.car.sales.service;

import md.car.sales.dto.CountryDTO;
import md.car.sales.model.Country;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CountryService {

    List<CountryDTO> getCountries();

    Country getCountryById(long id);

    Country getCountryByName(String countryName);

    void addCountries(List<Country> countries);
}
