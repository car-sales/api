package md.car.sales.service;

import org.springframework.stereotype.Service;

@Service
public interface StatisticService {

    void addViewToAd(long adId);

    void incAddedAds();

    int getAdViews(long id);

    int getTodayAdViews(long id);

    int getAddedAdsCount();
}
