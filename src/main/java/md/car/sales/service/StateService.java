package md.car.sales.service;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StateService {
    List<String> getStates();
}
