package md.car.sales.service;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ColorService {
    List<String> getColors();
}
