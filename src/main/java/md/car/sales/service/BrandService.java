package md.car.sales.service;

import md.car.sales.dto.BrandDTO;
import md.car.sales.model.Brand;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BrandService {

    List<BrandDTO> getBrands();

    Brand getBrandByBrandName(String brandName);

    List<BrandDTO> addBrands(List<Brand> brands);

}
