package md.car.sales.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ConflictException extends RuntimeException {

    public ConflictException(String message) {
        super(message, null, true, false);
    }

    public ConflictException(String message, Throwable cause) {
        super(message, cause);
    }

}
