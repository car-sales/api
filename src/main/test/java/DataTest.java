import md.car.sales.dto.BrandDTO;
import md.car.sales.dto.CountryDTO;
import md.car.sales.model.Comfort;
import md.car.sales.model.Security;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DataTest extends AbstractTest {

    @PostConstruct
    private void init() {
        this.setUp();
    }

    // Test receiving data

    @Test
    public void getFuels_receivingTheFuelsList_NotNull() throws Exception {
        MvcResult mvcResult = doRequest("/fuels");
        assertStatusCode(mvcResult, 200);
        assertDataNotNull(mvcResult, String[].class);
    }

    @Test
    public void getBrands_receivingTheBrandsList_NotNull() throws Exception {

        MvcResult mvcResult = doRequest("/brands");
        assertStatusCode(mvcResult, 200);
        assertDataNotNull(mvcResult, BrandDTO[].class);
    }

    @Test
    public void getBodyStyles_receivingTheBodyStylesList_NotNull() throws Exception {

        MvcResult mvcResult = doRequest("/bodyStyles");
        assertStatusCode(mvcResult, 200);
        assertDataNotNull(mvcResult, String[].class);
    }

    @Test
    public void getStates_receivingTheStatesList_NotNull() throws Exception {

        MvcResult mvcResult = doRequest("/states");
        assertStatusCode(mvcResult, 200);
        assertDataNotNull(mvcResult, String[].class);
    }

    @Test
    public void getColors_receivingTheColorsList_NotNull() throws Exception {

        MvcResult mvcResult = doRequest("/colors");
        assertStatusCode(mvcResult, 200);
        assertDataNotNull(mvcResult, String[].class);
    }

    @Test
    public void getCountries_receivingTheCountriesList_NotNull() throws Exception {

        MvcResult mvcResult = doRequest("/countries");
        assertStatusCode(mvcResult, 200);
        assertDataNotNull(mvcResult, CountryDTO[].class);
    }

    @Test
    public void getComforts_receivingTheComfortsList_NotNull() throws Exception {

        MvcResult mvcResult = doRequest("/comforts");
        assertStatusCode(mvcResult, 200);
        assertDataNotNull(mvcResult, Comfort[].class);
    }

    @Test
    public void getSecurities_receivingTheSecuritiesList_NotNull() throws Exception {

        MvcResult mvcResult = doRequest("/securities");
        assertStatusCode(mvcResult, 200);
        assertDataNotNull(mvcResult, Security[].class);
    }

    @Test
    public void getTraction_receivingTheTractionList_NotNull() throws Exception {

        MvcResult mvcResult = doRequest("/traction");
        assertStatusCode(mvcResult, 200);
        assertDataNotNull(mvcResult, String[].class);
    }

    @Test
    public void getTransmissions_receivingTheTransmissionsList_NotNull() throws Exception {

        MvcResult mvcResult = doRequest("/transmissions");
        assertStatusCode(mvcResult, 200);
        assertDataNotNull(mvcResult, String[].class);
    }

    @Test
    public void getDealTypes_receivingTheDealTypesList_NotNull() throws Exception {

        MvcResult mvcResult = doRequest("/dealTypes");
        assertStatusCode(mvcResult, 200);
        assertDataNotNull(mvcResult, String[].class);
    }

    @Test
    public void getCurrencies_receivingTheCurrenciesList_NotNull() throws Exception {

        MvcResult mvcResult = doRequest("/currencies");
        assertStatusCode(mvcResult, 200);
        assertDataNotNull(mvcResult, String[].class);
    }

    //Test content of received data

    @Test
    public void getFuels_receivingTheFuelsList_ValidContent() throws Exception {

        MvcResult mvcResult = doRequest("/fuels");

        String result = mvcResult.getResponse().getContentAsString();
        String expectedResult = "[\"Hybrid\",\"Plug-in Hybrid\",\"Gasoline\",\"Diesel\",\"Electric\",\"Gas\"]";

        assertEquals(expectedResult, result);
    }

    @Test
    public void getBrands_receivingTheBrandsList_ValidContent() throws Exception {

        MvcResult mvcResult = doRequest("/brands");
        List<BrandDTO> result = Arrays.asList(getRequestContent(mvcResult, BrandDTO[].class));

        assertTrue(result.size() > 10);

        result.forEach(brand -> {
            assertTrue(brand.getModels().size() > 1);
        });
    }

    @Test
    public void getBodyStyles_receivingTheBodyStylesList_ValidContent() throws Exception {

        MvcResult mvcResult = doRequest("/bodyStyles");
        String[] result = getRequestContent(mvcResult, String[].class);

        assertEquals(10, result.length);
    }

    @Test
    public void getStates_receivingTheStatesList_ValidContent() throws Exception {

        MvcResult mvcResult = doRequest("/states");
        String result = mvcResult.getResponse().getContentAsString();
        String expectedResult = "[\"Used\",\"New\",\"Needs repair\"]";

        assertEquals(expectedResult, result);
    }

    @Test
    public void getColors_receivingTheColorsList_ValidContent() throws Exception {

        MvcResult mvcResult = doRequest("/colors");
        String result = mvcResult.getResponse().getContentAsString();
        String expectedResult = "[\"Beige\",\"Black\",\"Blue\",\"Brown\",\"Gold\",\"Gray\",\"Green\",\"Orange\",\"Pink\",\"Purple\",\"Red\",\"Silver\",\"White\",\"Yellow\",\"Other\"]";

        assertEquals(expectedResult, result);
    }

    @Test
    public void getCountries_receivingTheCountriesList_ValidContent() throws Exception {

        MvcResult mvcResult = doRequest("/countries");
        List<CountryDTO> result = Arrays.asList(getRequestContent(mvcResult, CountryDTO[].class));

        assertEquals("Romania", result.get(0).getCountryName());
        assertEquals("Moldova", result.get(1).getCountryName());

        result.forEach(brand -> {
            assertTrue(brand.getRegions().size() > 20);
        });
    }

    @Test
    public void getComforts_receivingTheComfortsList_ValidContent() throws Exception {

        MvcResult mvcResult = doRequest("/comforts");
        Comfort[] result = getRequestContent(mvcResult, Comfort[].class);

        assertTrue(result.length > 5);
    }

    @Test
    public void getSecurities_receivingTheSecuritiesList_ValidContent() throws Exception {

        MvcResult mvcResult = doRequest("/securities");
        Security[] result = getRequestContent(mvcResult, Security[].class);

        assertTrue(result.length > 5);
    }

    @Test
    public void getTraction_receivingTheTractionList_ValidContent() throws Exception {

        MvcResult mvcResult = doRequest("/traction");

        String result = mvcResult.getResponse().getContentAsString();
        String expectedResult = "[\"AWD\",\"RWD\",\"FWD\",\"4x2\",\"4x4\"]";

        assertEquals(expectedResult, result);
    }

    @Test
    public void getTransmissions_receivingTheTransmissionsList_ValidContent() throws Exception {

        MvcResult mvcResult = doRequest("/transmissions");

        String result = mvcResult.getResponse().getContentAsString();
        String expectedResult = "[\"Automatic\",\"Manual\",\"Auto-manual\",\"CVT\",\"Other\"]";

        assertEquals(expectedResult, result);
    }

    @Test
    public void getDealTypes_receivingTheDealTypesList_ValidContent() throws Exception {

        MvcResult mvcResult = doRequest("/dealTypes");

        String result = mvcResult.getResponse().getContentAsString();
        String expectedResult = "[\"Sale\",\"Buy\",\"Change\"]";

        assertEquals(expectedResult, result);
    }

    @Test
    public void getCurrencies_receivingTheCurrenciesList_ValidContent() throws Exception {

        MvcResult mvcResult = doRequest("/currencies");

        String result = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8);
        String expectedResult = "[\"$\",\"€\",\"MDL\",\"RON\"]";

        assertEquals(expectedResult, result);
    }

}
