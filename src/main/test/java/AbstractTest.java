import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import md.car.sales.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public abstract class AbstractTest {
    protected MockMvc mvc;

    @Autowired
    WebApplicationContext webApplicationContext;

    protected void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    protected <T> T mapFromJson(String json, Class<T> clazz) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, clazz);
    }

    protected MvcResult doRequest(String uri) {
        try {
            return mvc.perform(MockMvcRequestBuilders.get(uri)
                    .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void assertStatusCode(MvcResult mvcResult, int code) {
        int status = mvcResult.getResponse().getStatus();
        assertEquals(code, status);
    }

    protected <T> void assertDataNotNull(MvcResult mvcResult, Class<T> clazz) throws IOException {
        String content = mvcResult.getResponse().getContentAsString();
        assertNotNull(this.mapFromJson(content, clazz));
    }

    protected <T> T getRequestContent(MvcResult mvcResult, Class<T> clazz) throws IOException {
        String content = mvcResult.getResponse().getContentAsString();
        return this.mapFromJson(content, clazz);
    }
}