import md.car.sales.dto.AdsPage;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;

import javax.annotation.PostConstruct;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AdControllerTest extends AbstractTest {

    @PostConstruct
    private void init() {
        this.setUp();
    }

    @Test
    public void getAds() throws Exception {

        MvcResult mvcResult = doRequest("/ads");

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        AdsPage page = super.mapFromJson(content, AdsPage.class);
        assertEquals(24, page.getPageSize());
    }
}
